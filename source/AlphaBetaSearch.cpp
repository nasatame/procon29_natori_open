#include <bitset>
#include <algorithm>
#include <unordered_map>
#include <utility>
#include <queue>
#include "Computer.hpp"

//一番頑張って作ったのにbeamSearchより弱かった
Procon29::Computer::ResultType Procon29::Computer::Search::searchAlphaBeta(const Teams & teams, const Field & field, const Evaluate & evaluate)
{
	const int search_depth = std::min(2 * 2, 2 * (MaxTurn - (evaluate.game_turn)));
	const size_t my_team = evaluate.my_team == TeamType::MyTeam ? 0 : 1;
	const size_t enemy_team = evaluate.my_team == TeamType::EnemyTeam ? 0 : 1;

	int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < field.boardSizeGetter().x && p.y < field.boardSizeGetter().y);
	};

	auto convXY = [](const Point& p) {
		assert(0 <= p.x + p.y * 12 && p.x + p.y * 12 < 144);
		return p.x + p.y * 12;
	};

	InfoAlphaBeta first_state;
	first_state.first_dir[0] = -1;
	first_state.first_dir[1] = -1;
	first_state.my_x[0] = teams[my_team].agentsGeter()[0].nowPoint.x;
	first_state.my_y[0] = teams[my_team].agentsGeter()[0].nowPoint.y;
	first_state.my_x[1] = teams[my_team].agentsGeter()[1].nowPoint.x;
	first_state.my_y[1] = teams[my_team].agentsGeter()[1].nowPoint.y;
	first_state.enemy_x[0] = teams[enemy_team].agentsGeter()[0].nowPoint.x;
	first_state.enemy_y[0] = teams[enemy_team].agentsGeter()[0].nowPoint.y;
	first_state.enemy_x[1] = teams[enemy_team].agentsGeter()[1].nowPoint.x;
	first_state.enemy_y[1] = teams[enemy_team].agentsGeter()[1].nowPoint.y;
	for (auto i : step(field.boardSizeGetter())) {
		if (field.boardGetter().at(i).teamType != TeamType::None && field.boardGetter().at(i).teamType != evaluate.my_team) {
			first_state.enemy_area.set(convXY(i));
		}
		if (field.boardGetter().at(i).teamType == evaluate.my_team) {
			first_state.my_area.set(convXY(i));
		}
	}

	first_state.my_area_point = calcScore(field, first_state.my_area);
	first_state.enemy_area_point = calcScore(field, first_state.enemy_area);

	first_state.depth = search_depth;

	GlobalAlphaBeta global;

	global.board_size_y = field.boardSizeGetter().y;
	global.board_size_x = field.boardSizeGetter().x;

	global.alpha = -100000;
	global.beta = 100000;

	global.search_depth = search_depth / 2;
	global.game_turn = evaluate.game_turn;

	for (int i = 0; i < MaxFieldY; i++)
		std::fill(global.field[i], global.field[i] + MaxFieldX, -32);

	for (int i = 0; i < field.boardSizeGetter().y; i++)
		for (int j = 0; j < field.boardSizeGetter().x; j++) {
			global.field[i][j] = field.boardGetter()[i][j].score;
		}

	ResultAlphaBeta optimal;

	Array<InfoAlphaBeta> children;

	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {

			auto now_state = first_state;

			Point next_point[2] = {
				Point(now_state.my_x[0] + dxy[dxy1], now_state.my_y[0] + dxy[dxy1 + 1]),
				Point(now_state.my_x[1] + dxy[dxy2], now_state.my_y[1] + dxy[dxy2 + 1]) };

			if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
				continue;

			//同じところに移動禁止
			if (next_point[0] == next_point[1])
				continue;

			//敵チームがいるところ禁止いれるか？
			//ステイと同じ原理で禁止
			if (next_point[0] == Point(now_state.enemy_x[0], now_state.enemy_y[0]) ||
				next_point[0] == Point(now_state.enemy_x[1], now_state.enemy_y[1]) ||
				next_point[1] == Point(now_state.enemy_x[0], now_state.enemy_y[0]) ||
				next_point[1] == Point(now_state.enemy_x[1], now_state.enemy_y[1]))
				continue;

			now_state.depth -= 1;
			now_state.first_dir[0] = dxy1;
			now_state.first_dir[1] = dxy2;

			//4パターン行動可能性がある
			//4^2で16パターンさすがに列挙はダルい
			{
				for (int pattern = 0; pattern < 16; pattern++) {

					auto tmp_state = now_state;
					bool skipFlag = false;
					int p = pattern;
					const int pd4 = p % 4;

					for (int i = 0; i < MaxAgentNum; i++) {
						if (pd4 == 0) {
							if (tmp_state.my_area[convXY(next_point[i])] == 1) {
								tmp_state.my_x[i] = next_point[i].x;
								tmp_state.my_y[i] = next_point[i].y;
								tmp_state.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 1) {
							if (tmp_state.my_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
								tmp_state.my_area[convXY(next_point[i])] = 0;
								tmp_state.my_area_point = calcScore(global.field, tmp_state.my_area);
								tmp_state.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 2) {
							if (tmp_state.enemy_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] > 0) {
								tmp_state.enemy_area[convXY(next_point[i])] = 0;
								tmp_state.enemy_area_point = calcScore(global.field, tmp_state.enemy_area);
								tmp_state.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 3) {
							if (tmp_state.my_area[convXY(next_point[i])] == 0 && tmp_state.enemy_area[convXY(next_point[i])] == 0) {
								tmp_state.my_x[i] = next_point[i].x;
								tmp_state.my_y[i] = next_point[i].y;
								tmp_state.my_area[convXY(next_point[i])] = 1;
								tmp_state.my_area_point = calcScore(global.field, tmp_state.my_area);
								tmp_state.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						p >>= 2;
					}

					if (skipFlag)
						continue;

					children << tmp_state;
				}
			}

		}
	}

	//並び替え

	sort(children.begin(), children.end(), [](const InfoAlphaBeta& left, const InfoAlphaBeta& right) {
		return (left.my_area_point - left.enemy_area_point) > (right.my_area_point - right.enemy_area_point);
	});

	//高速化の工夫
	//並列化

	const size_t core_num = 6;
	const size_t fast_size = 3;
	size_t task_size = children.size() - fast_size;
	Array<std::future<ResultAlphaBeta>> arr_result(core_num);

	for (int i = 0; i < fast_size; i++) {
		optimal = enemyTeamAlphaBetaAsync(children[i], global);
		if(optimal.score > global.alpha)
			global.alpha = optimal.score;
	}

	for (int loop = 0; loop < task_size / core_num; loop++) {
		for (int i = loop * core_num; i < std::min(task_size, (loop + 1) * core_num); i++) {
			arr_result[i - loop * core_num] = (std::async(std::launch::async,
				[](InfoAlphaBeta info, GlobalAlphaBeta global) {return Search().enemyTeamAlphaBeta(info, global); },
				children[i], global));
		}
		for (int i = 0; i < core_num; i++) {
			ResultAlphaBeta result = arr_result[i].get();
			if (result.score > optimal.score) {
				optimal = result;
				global.alpha = optimal.score;
			}
		}
	}

	//double parent_beta = global.beta;

	////探索、およびalpha値、beta値の設定
	//for (int i = 0; i < children.size(); i++) {

	//	ResultAlphaBeta result = enemyTeamAlphaBeta(children[i], global);

	//	if (result.score > optimal.score) {
	//		optimal = result;
	//		global.alpha = optimal.score;
	//	}
	//}


	ResultType result;
	{
		Array<Vec3> first_move_point = { Vec3(teams[0].agentsGeter()[0].nowPoint + Point(dxy[optimal.first_dir[0]],dxy[optimal.first_dir[0] + 1]), optimal.action[0]),
			Vec3(teams[0].agentsGeter()[1].nowPoint + Point(dxy[optimal.first_dir[1]],dxy[optimal.first_dir[1] + 1]),optimal.action[1]) };

		std::tuple<Array<Vec3>, double> candidate = {
			first_move_point,
			optimal.score };
		result << candidate;

	}
	return result;
}

Procon29::Computer::Search::ResultAlphaBeta Procon29::Computer::Search::enemyTeamAlphaBeta(const InfoAlphaBeta& info, GlobalAlphaBeta& global)
{

	const int search_depth = info.depth;

	int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < global.board_size_x && p.y < global.board_size_y);
	};

	auto convXY = [](const Point& p) {
		assert(0 <= p.x + p.y * 12 && p.x + p.y * 12 < 144);
		return p.x + p.y * 12;
	};

	InfoAlphaBeta first_state = info;

	ResultAlphaBeta optimal;
	optimal.score = 100000;

	//場合によってはhash関数

	Array<InfoAlphaBeta> children;

	/*
	{
	const size_t core_num = 9;
	Array<std::future<Array<InfoAlphaBeta>>> arr_result(core_num);

	for (int loop = 0; loop < core_num; loop++) {
	arr_result[loop] = (std::async(std::launch::async,
	[this, loop, core_num](InfoAlphaBeta info, GlobalAlphaBeta global) {

	const int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	const auto checkIn = [=](const Point& p) {
	return (0 <= p.x && 0 <= p.y && p.x < global.board_size_x && p.y < global.board_size_y);
	};

	const auto convXY = [](const Point& p) {
	assert(0 <= p.x + p.y * 12 && p.x + p.y * 12 < 144);
	return p.x + p.y * 12;
	};

	Array<InfoAlphaBeta> result;

	//列挙
	for (int dxy1 = loop * (9 / core_num); dxy1 < (loop + 1) * (9 / core_num); dxy1++) {
	for (int dxy2 = 0; dxy2 < 9; dxy2++) {

	auto now_state = info;

	Point next_point[2] = {
	Point(now_state.enemy_x[0] + dxy[dxy1], now_state.enemy_y[0] + dxy[dxy1 + 1]),
	Point(now_state.enemy_x[1] + dxy[dxy2], now_state.enemy_y[1] + dxy[dxy2 + 1]) };

	if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
	continue;

	//同じところに移動禁止
	if (next_point[0] == next_point[1])
	continue;

	//敵チームがいるところ禁止いれるか？
	//ステイと同じ原理で禁止
	if (next_point[0] == Point(now_state.my_x[0], now_state.my_y[0]) ||
	next_point[0] == Point(now_state.my_x[1], now_state.my_y[1]) ||
	next_point[1] == Point(now_state.my_x[0], now_state.my_y[0]) ||
	next_point[1] == Point(now_state.my_x[1], now_state.my_y[1]))
	continue;


	now_state.depth -= 1;
	//4パターン行動可能性がある
	//4^2で16パターンさすがに列挙はダルい
	{
	for (int pattern = 0; pattern < 16; pattern++) {

	auto tmp_state = now_state;
	bool skipFlag = false;
	int p = pattern;
	const int pd4 = p % 4;

	for (int i = 0; i < MaxAgentNum; i++) {
	if (pd4 == 0) {
	if (tmp_state.enemy_area[convXY(next_point[i])] == 1) {
	tmp_state.enemy_x[i] = next_point[i].x;
	tmp_state.enemy_y[i] = next_point[i].y;
	}
	else {
	skipFlag = true;
	break;
	}
	}
	if (pd4 == 1) {
	if (tmp_state.enemy_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
	tmp_state.enemy_area[convXY(next_point[i])] = 0;
	//tmp_state.enemy_area_point = this->calcScore(global.field, tmp_state.enemy_area);
	tmp_state.enemy_area_point -= global.field[next_point[i].y][next_point[i].x];
	}
	else {
	skipFlag = true;
	break;
	}
	}
	if (pd4 == 2) {
	if (tmp_state.my_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] > 0) {
	tmp_state.my_area[convXY(next_point[i])] = 0;
	//tmp_state.my_area_point = calcScore(global.field, tmp_state.my_area);
	tmp_state.my_area_point -= global.field[next_point[i].y][next_point[i].x];
	}
	else {
	skipFlag = true;
	break;
	}
	}
	if (pd4 == 3) {
	if (tmp_state.my_area[convXY(next_point[i])] == 0 && tmp_state.enemy_area[convXY(next_point[i])] == 0) {
	tmp_state.enemy_x[i] = next_point[i].x;
	tmp_state.enemy_y[i] = next_point[i].y;
	tmp_state.enemy_area[convXY(next_point[i])] = 1;
	//tmp_state.enemy_area_point = calcScore(global.field, tmp_state.enemy_area);
	tmp_state.enemy_area_point += global.field[next_point[i].y][next_point[i].x];
	}
	else {
	skipFlag = true;
	break;
	}
	}
	p >>= 2;
	}

	if (skipFlag)
	continue;

	result << tmp_state;
	}
	}

	}
	}
	return result;
	},
	first_state, global));
	}

	for (int i = 0; i < core_num; i++) {
	Array<InfoAlphaBeta> result = arr_result[i].get();
	for (int j = 0; j < result.size(); j++)
	children.push_back(std::move(result[j]));
	}
	}
	*/

	//列挙
	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {

			auto now_state = first_state;

			Point next_point[2] = {
				Point(now_state.enemy_x[0] + dxy[dxy1], now_state.enemy_y[0] + dxy[dxy1 + 1]),
				Point(now_state.enemy_x[1] + dxy[dxy2], now_state.enemy_y[1] + dxy[dxy2 + 1]) };

			if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
				continue;

			//同じところに移動禁止
			if (next_point[0] == next_point[1])
				continue;

			//敵チームがいるところ禁止いれるか？
			//ステイと同じ原理で禁止
			if (next_point[0] == Point(now_state.my_x[0], now_state.my_y[0]) ||
				next_point[0] == Point(now_state.my_x[1], now_state.my_y[1]) ||
				next_point[1] == Point(now_state.my_x[0], now_state.my_y[0]) ||
				next_point[1] == Point(now_state.my_x[1], now_state.my_y[1]))
				continue;


			now_state.depth -= 1;
			//4パターン行動可能性がある
			//4^2で16パターンさすがに列挙はダルい
			{
				for (int pattern = 0; pattern < 16; pattern++) {

					auto tmp_state = now_state;
					bool skipFlag = false;
					int p = pattern;
					const int pd4 = p % 4;

					for (int i = 0; i < MaxAgentNum; i++) {
						if (pd4 == 0) {
							if (tmp_state.enemy_area[convXY(next_point[i])] == 1) {
								tmp_state.enemy_x[i] = next_point[i].x;
								tmp_state.enemy_y[i] = next_point[i].y;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 1) {
							if (tmp_state.enemy_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
								tmp_state.enemy_area[convXY(next_point[i])] = 0;
								//tmp_state.enemy_area_point = calcScore(global.field, tmp_state.enemy_area);
								tmp_state.enemy_area_point -= global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 2) {
							if (tmp_state.my_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] > 0) {
								tmp_state.my_area[convXY(next_point[i])] = 0;
								//tmp_state.my_area_point = calcScore(global.field, tmp_state.my_area);
								tmp_state.my_area_point -= global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 3) {
							if (tmp_state.my_area[convXY(next_point[i])] == 0 && tmp_state.enemy_area[convXY(next_point[i])] == 0) {
								tmp_state.enemy_x[i] = next_point[i].x;
								tmp_state.enemy_y[i] = next_point[i].y;
								tmp_state.enemy_area[convXY(next_point[i])] = 1;
								//tmp_state.enemy_area_point = calcScore(global.field, tmp_state.enemy_area);
								tmp_state.enemy_area_point += global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						p >>= 2;
					}

					if (skipFlag)
						continue;

					children << tmp_state;
				}
			}

		}
	}


	//並び替え

	sort(children.begin(), children.end(), [](const InfoAlphaBeta& left, const InfoAlphaBeta& right) {
		return (left.my_area_point - left.enemy_area_point) < (right.my_area_point - right.enemy_area_point);
	});


	double parent_alpha = global.alpha;

	//探索、およびalpha値、beta値の設定
	for (int i = 0; i < children.size(); i++) {

		//null window search？
		//NegaScout必須ポイ
		//最小のbetaを取得・更新
		//これ単に下界取得で代用可能？
		//無理だと思われる。
		//つまりこの枝以下で更新（optimalよりも小さい値が返る）の機会があるかを確かめたい。
		//ほぼほぼ効かない。大きく値が変わる可能性があるから。

		//通常のalpha beta search

		ResultAlphaBeta result = myTeamAlphaBeta(children[i], global);

		if (result.score < optimal.score) {
			optimal = result;
			global.beta = optimal.score;
		}

		if (global.beta < parent_alpha) {
			return optimal;
		}
	}

	return optimal;
}

Procon29::Computer::Search::ResultAlphaBeta Procon29::Computer::Search::enemyTeamAlphaBetaAsync(const InfoAlphaBeta & info, GlobalAlphaBeta & global)
{

	const int search_depth = info.depth;

	int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < global.board_size_x && p.y < global.board_size_y);
	};

	auto convXY = [](const Point& p) {
		assert(0 <= p.x + p.y * 12 && p.x + p.y * 12 < 144);
		return p.x + p.y * 12;
	};

	InfoAlphaBeta first_state = info;

	ResultAlphaBeta optimal;
	optimal.score = 100000;

	//場合によってはhash関数

	Array<InfoAlphaBeta> children;

	//列挙
	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {

			auto now_state = first_state;

			Point next_point[2] = {
				Point(now_state.enemy_x[0] + dxy[dxy1], now_state.enemy_y[0] + dxy[dxy1 + 1]),
				Point(now_state.enemy_x[1] + dxy[dxy2], now_state.enemy_y[1] + dxy[dxy2 + 1]) };

			if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
				continue;

			//同じところに移動禁止
			if (next_point[0] == next_point[1])
				continue;

			//敵チームがいるところ禁止いれるか？
			//ステイと同じ原理で禁止
			if (next_point[0] == Point(now_state.my_x[0], now_state.my_y[0]) ||
				next_point[0] == Point(now_state.my_x[1], now_state.my_y[1]) ||
				next_point[1] == Point(now_state.my_x[0], now_state.my_y[0]) ||
				next_point[1] == Point(now_state.my_x[1], now_state.my_y[1]))
				continue;


			now_state.depth -= 1;
			//4パターン行動可能性がある
			//4^2で16パターンさすがに列挙はダルい
			{
				for (int pattern = 0; pattern < 16; pattern++) {

					auto tmp_state = now_state;
					bool skipFlag = false;
					int p = pattern;
					const int pd4 = p % 4;

					for (int i = 0; i < MaxAgentNum; i++) {
						if (pd4 == 0) {
							if (tmp_state.enemy_area[convXY(next_point[i])] == 1) {
								tmp_state.enemy_x[i] = next_point[i].x;
								tmp_state.enemy_y[i] = next_point[i].y;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 1) {
							if (tmp_state.enemy_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
								tmp_state.enemy_area[convXY(next_point[i])] = 0;
								//tmp_state.enemy_area_point = calcScore(global.field, tmp_state.enemy_area);
								tmp_state.enemy_area_point -= global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 2) {
							if (tmp_state.my_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] > 0) {
								tmp_state.my_area[convXY(next_point[i])] = 0;
								//tmp_state.my_area_point = calcScore(global.field, tmp_state.my_area);
								tmp_state.my_area_point -= global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 3) {
							if (tmp_state.my_area[convXY(next_point[i])] == 0 && tmp_state.enemy_area[convXY(next_point[i])] == 0) {
								tmp_state.enemy_x[i] = next_point[i].x;
								tmp_state.enemy_y[i] = next_point[i].y;
								tmp_state.enemy_area[convXY(next_point[i])] = 1;
								//tmp_state.enemy_area_point = calcScore(global.field, tmp_state.enemy_area);
								tmp_state.enemy_area_point += global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						p >>= 2;
					}

					if (skipFlag)
						continue;

					children << tmp_state;
				}
			}

		}
	}


	//並び替え

	sort(children.begin(), children.end(), [](const InfoAlphaBeta& left, const InfoAlphaBeta& right) {
		return (left.my_area_point - left.enemy_area_point) < (right.my_area_point - right.enemy_area_point);
	});

	double parent_alpha = global.alpha;

	const size_t core_num = 6;
	size_t task_size = children.size();
	Array<std::future<ResultAlphaBeta>> arr_result(core_num);

	for (int loop = 0; loop < task_size / core_num; loop++) {
		for (int i = loop * core_num; i < std::min(task_size, (loop + 1) * core_num); i++) {
			arr_result[i - loop * core_num] = (std::async(std::launch::async,
				[](InfoAlphaBeta info, GlobalAlphaBeta global) {return Search().myTeamAlphaBeta(info, global); },
				children[i], global));
		}
		for (int i = 0; i < core_num; i++) {
			ResultAlphaBeta result = arr_result[i].get();

			if (result.score < optimal.score) {
				optimal = result;
				global.beta = optimal.score;
			}
		}
	}
	return optimal;
}

Procon29::Computer::Search::ResultAlphaBeta Procon29::Computer::Search::myTeamAlphaBeta(const InfoAlphaBeta & info, GlobalAlphaBeta & global)
{
	const int search_depth = info.depth;
	const double beam_demerit = 0.3;

	int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < global.board_size_x && p.y < global.board_size_y);
	};

	auto convXY = [](const Point& p) {
		assert(0 <= p.x + p.y * 12 && p.x + p.y * 12 < 144);
		return p.x + p.y * 12;
	};

	//終了条件
	if (search_depth <= 0) {
		ResultAlphaBeta result;
		result.score = calcScore(global.field, info.my_area) - calcScore(global.field, info.enemy_area);
		//周辺探索
		//ビームサーチ
		auto f_my = std::async(std::launch::async,
			[](InfoAlphaBeta info, GlobalAlphaBeta global) {
			InfoBeamSearch beam_info;
			beam_info.enemy_area = info.enemy_area;
			beam_info.my_area = info.my_area;

			for (int i = 0; i < MaxAgentNum; i++) {
				beam_info.x[i] = info.my_x[i];
				beam_info.y[i] = info.my_y[i];
			}

			Point enemyPos[2] = { Point(info.enemy_x[0],info.enemy_y[0]),Point(info.enemy_x[1],info.enemy_y[1]) };

			return Search().evaluateBeamSearch(beam_info, global, enemyPos);
		},
			info, global);

		auto f_enemy = std::async(std::launch::async,
			[](InfoAlphaBeta info, GlobalAlphaBeta global) {
			InfoBeamSearch beam_info;
			beam_info.enemy_area = info.my_area;
			beam_info.my_area = info.enemy_area;

			for (int i = 0; i < MaxAgentNum; i++) {
				beam_info.x[i] = info.enemy_x[i];
				beam_info.y[i] = info.enemy_y[i];
			}

			Point enemyPos[2] = { Point(info.my_x[0],info.my_y[0]),Point(info.my_x[1],info.my_y[1]) };

			return Search().evaluateBeamSearch(beam_info, global, enemyPos);
		},
			info, global);

		{
			result.score += f_my.get() * beam_demerit;
			result.score -= f_enemy.get() * beam_demerit;
		}

		result.first_dir[0] = info.first_dir[0];
		result.first_dir[1] = info.first_dir[1];
		return result;
	}

	ResultAlphaBeta optimal;
	//枝狩り
	/*if (info.my_area_point < info.enemy_area_point - 16 * global.board_size_x * global.board_size_y / 20.0) {
	optimal.score = 100000;
	return optimal;
	}*/

	InfoAlphaBeta first_state = info;

	//場合によってはhash関数

	Array<InfoAlphaBeta> children;
	//列挙
	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {

			auto now_state = info;

			Point next_point[2] = {
				Point(now_state.my_x[0] + dxy[dxy1], now_state.my_y[0] + dxy[dxy1 + 1]),
				Point(now_state.my_x[1] + dxy[dxy2], now_state.my_y[1] + dxy[dxy2 + 1]) };

			if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
				continue;

			//同じところに移動禁止
			if (next_point[0] == next_point[1])
				continue;

			//敵チームがいるところ禁止いれるか？
			//ステイと同じ原理で禁止
			if (next_point[0] == Point(now_state.enemy_x[0], now_state.enemy_y[0]) ||
				next_point[0] == Point(now_state.enemy_x[1], now_state.enemy_y[1]) ||
				next_point[1] == Point(now_state.enemy_x[0], now_state.enemy_y[0]) ||
				next_point[1] == Point(now_state.enemy_x[1], now_state.enemy_y[1]))
				continue;

			now_state.depth -= 1;

			//4パターン行動可能性がある
			//4^2で16パターンさすがに列挙はダルい
			{
				for (int pattern = 0; pattern < 16; pattern++) {

					auto tmp_state = now_state;
					bool skipFlag = false;
					int p = pattern;
					const int pd4 = p % 4;

					for (int i = 0; i < MaxAgentNum; i++) {
						if (pd4 == 0) {
							if (tmp_state.my_area[convXY(next_point[i])] == 1) {
								tmp_state.my_x[i] = next_point[i].x;
								tmp_state.my_y[i] = next_point[i].y;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 1) {
							if (tmp_state.my_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
								tmp_state.my_area[convXY(next_point[i])] = 0;
								//tmp_state.my_area_point = calcScore(global.field, tmp_state.my_area);
								tmp_state.my_area_point -= global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 2) {
							if (tmp_state.enemy_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] > 0) {
								tmp_state.enemy_area[convXY(next_point[i])] = 0;
								//tmp_state.enemy_area_point = calcTileScore(global.field, tmp_state.enemy_area);
								tmp_state.enemy_area_point -= global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						if (pd4 == 3) {
							if (tmp_state.my_area[convXY(next_point[i])] == 0 && tmp_state.enemy_area[convXY(next_point[i])] == 0) {
								tmp_state.my_x[i] = next_point[i].x;
								tmp_state.my_y[i] = next_point[i].y;
								tmp_state.my_area[convXY(next_point[i])] = 1;
								//tmp_state.my_area_point = calcTileScore(global.field, tmp_state.my_area);
								tmp_state.my_area_point += global.field[next_point[i].y][next_point[i].x];
							}
							else {
								skipFlag = true;
								break;
							}
						}
						p >>= 2;
					}

					if (skipFlag)
						continue;

					children << tmp_state;
				}
			}
		}
	}


	/*

	{
	const size_t core_num = 9;
	Array<std::future<Array<InfoAlphaBeta>>> arr_result(core_num);

	for (int loop = 0; loop < core_num; loop++) {
	arr_result[loop] = (std::async(std::launch::async,
	[this, loop, core_num](InfoAlphaBeta info, GlobalAlphaBeta global) {

	const int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	const auto checkIn = [=](const Point& p) {
	return (0 <= p.x && 0 <= p.y && p.x < global.board_size_x && p.y < global.board_size_y);
	};

	const auto convXY = [](const Point& p) {
	assert(0 <= p.x + p.y * 12 && p.x + p.y * 12 < 144);
	return p.x + p.y * 12;
	};

	Array<InfoAlphaBeta> result;

	//列挙
	for (int dxy1 = loop * (9 / core_num); dxy1 < (loop + 1) * (9 / core_num); dxy1++) {
	for (int dxy2 = 0; dxy2 < 9; dxy2++) {

	auto now_state = info;

	Point next_point[2] = {
	Point(now_state.my_x[0] + dxy[dxy1], now_state.my_y[0] + dxy[dxy1 + 1]),
	Point(now_state.my_x[1] + dxy[dxy2], now_state.my_y[1] + dxy[dxy2 + 1]) };

	if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
	continue;

	//同じところに移動禁止
	if (next_point[0] == next_point[1])
	continue;

	//敵チームがいるところ禁止いれるか？
	//ステイと同じ原理で禁止
	if (next_point[0] == Point(now_state.enemy_x[0], now_state.enemy_y[0]) ||
	next_point[0] == Point(now_state.enemy_x[1], now_state.enemy_y[1]) ||
	next_point[1] == Point(now_state.enemy_x[0], now_state.enemy_y[0]) ||
	next_point[1] == Point(now_state.enemy_x[1], now_state.enemy_y[1]))
	continue;

	now_state.depth -= 1;

	//4パターン行動可能性がある
	//4^2で16パターンさすがに列挙はダルい
	{
	for (int pattern = 0; pattern < 16; pattern++) {

	auto tmp_state = now_state;
	bool skipFlag = false;
	int p = pattern;
	const int pd4 = p % 4;

	for (int i = 0; i < MaxAgentNum; i++) {
	if (pd4 == 0) {
	if (tmp_state.my_area[convXY(next_point[i])] == 1) {
	tmp_state.my_x[i] = next_point[i].x;
	tmp_state.my_y[i] = next_point[i].y;
	}
	else {
	skipFlag = true;
	break;
	}
	}
	if (pd4 == 1) {
	if (tmp_state.my_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
	tmp_state.my_area[convXY(next_point[i])] = 0;
	//tmp_state.my_area_point = calcScore(global.field, tmp_state.my_area);
	tmp_state.my_area_point -= global.field[next_point[i].y][next_point[i].x];
	}
	else {
	skipFlag = true;
	break;
	}
	}
	if (pd4 == 2) {
	if (tmp_state.enemy_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] > 0) {
	tmp_state.enemy_area[convXY(next_point[i])] = 0;
	//tmp_state.enemy_area_point = calcTileScore(global.field, tmp_state.enemy_area);
	tmp_state.enemy_area_point -= global.field[next_point[i].y][next_point[i].x];
	}
	else {
	skipFlag = true;
	break;
	}
	}
	if (pd4 == 3) {
	if (tmp_state.my_area[convXY(next_point[i])] == 0 && tmp_state.enemy_area[convXY(next_point[i])] == 0) {
	tmp_state.my_x[i] = next_point[i].x;
	tmp_state.my_y[i] = next_point[i].y;
	tmp_state.my_area[convXY(next_point[i])] = 1;
	//tmp_state.my_area_point = calcTileScore(global.field, tmp_state.my_area);
	tmp_state.my_area_point += global.field[next_point[i].y][next_point[i].x];
	}
	else {
	skipFlag = true;
	break;
	}
	}
	p >>= 2;
	}

	if (skipFlag)
	continue;

	result << tmp_state;
	}
	}
	}
	}

	return result;
	},
	first_state, global));
	}

	for (int i = 0; i < core_num; i++) {
	Array<InfoAlphaBeta> result = arr_result[i].get();
	for (int j = 0; j < result.size(); j++)
	children.push_back(std::move(result[j]));
	}
	}


	*/

	//並び替え

	sort(children.begin(), children.end(), [](const InfoAlphaBeta& left, const InfoAlphaBeta& right) {
		return (left.my_area_point - left.enemy_area_point) > (right.my_area_point - right.enemy_area_point);
	});


	double parent_beta = global.beta;

	//探索、およびalpha値、beta値の設定
	for (int i = 0; i < children.size(); i++) {

		ResultAlphaBeta result = enemyTeamAlphaBeta(children[i], global);

		if (result.score > optimal.score) {
			optimal = result;
			global.alpha = optimal.score;
		}

		if (global.alpha > parent_beta) {
			return optimal;
		}

	}

	return optimal;
}
