#pragma once

# include <Siv3D.hpp> // OpenSiv3D v0.2.6
# include "TeamType.hpp"

namespace Procon29 {

	struct Tile {
		int32 score;
		Procon29::TeamType teamType;
		bool exist;
		bool selecting;

		Tile(String initilizeString, bool isInvert);
		Tile();

		[[nodiscard]] String format() const;

		Procon29::TeamType teamTypeGetter() const;
	};

}