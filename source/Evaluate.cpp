#include <bitset>
#include <algorithm>
#include <unordered_map>
#include <utility>
#include <queue>
#include "Computer.hpp"

//モンテカルロ木的手法の評価関数として作った。結局beamSearchが強かった
double Procon29::Computer::Search::evaluateBeamSearch(const InfoBeamSearch & info, const GlobalAlphaBeta& global, const Point enemyPos[2])
{
	const size_t beam_size = 100;
	const int search_depth = global.search_depth;
	const int same_demerit = -30;
	const int was_moved_demerit = -5;
	const int wait_demerit = -10;
	const double diagonal_bonus = 1.5;
	const double fast_bonus = 2.0;
	const double enemy_peel_bonus = 0.9;
	const double my_area_merit = 0.4;
	const double enemy_area_merit = 0.8;
	const double first_turn_bonus = 0.5;
	const double last_turn_bonus = 3;
	const int minus_demerit = -2;
	const double fast_bonus_ratio = ((fast_bonus - 1.0) / search_depth);

	int dxy[9] = { 1,-1,-1,0,-1,1,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < global.board_size_x && p.y < global.board_size_y);
	};

	auto convXY = [](const Point& p) {
		return p.x + p.y * 12;
	};

	Array<InfoBeamSearch> nowBeamBucket;
	nowBeamBucket.reserve(beam_size);
	Array<InfoBeamSearch> nextBeamBucket;
	nowBeamBucket.reserve(beam_size * 100);

	InfoBeamSearch first_state = info;
	first_state.score = first_turn_bonus * (calcScore(global.field, first_state.my_area) - calcScore(global.field, first_state.enemy_area));
	nowBeamBucket << first_state;

	/*
	int average_div_3 = 0;
	for (int y = 0; y < global.board_size_y; y++) {
	for (int x = 0; x < global.board_size_x; x++) {
	if (global.field[y][x] > 0)
	average_div_3 += global.field[y][x];
	}
	}
	average_div_3 /= global.board_size_x * global.board_size_y;
	average_div_3 /= 3;
	if (average_div_3 == 0)
	average_div_3 = 1;
	*/

	for (int i = 0; i < search_depth; i++) {
		//enumlate
		for (int k = 0; k < nowBeamBucket.size(); k++) {
			auto& now_state = nowBeamBucket[k];
			for (int dxy1 = 0; dxy1 < 8; dxy1++) {
				Point agentPos1 = Point(now_state.x[0] + dxy[dxy1], now_state.y[0] + dxy[dxy1 + 1]);
				if (!checkIn(agentPos1))
					continue;
				if (global.field[agentPos1.y][agentPos1.x] <= 0) {
					continue;
				}
				for (int dxy2 = 0; dxy2 < 8; dxy2++) {
					Point agentPos2 = Point(now_state.x[1] + dxy[dxy2], now_state.y[1] + dxy[dxy2 + 1]);
					if (!checkIn(agentPos2))
						continue;
					if (global.field[agentPos2.y][agentPos2.x] <= 0) {
						continue;
					}
					Point next_point[2] = { agentPos1,agentPos2 };

					if (next_point[0] == next_point[1])
						continue;

					if (next_point[0] == enemyPos[0] ||
						next_point[0] == enemyPos[1] ||
						next_point[1] == enemyPos[0] ||
						next_point[1] == enemyPos[1])
						continue;


					InfoBeamSearch next_state;
					next_state.score = now_state.score;
					next_state.my_area = now_state.my_area;
					next_state.enemy_area = now_state.enemy_area;

					bool skipFlag = false;

					for (int agent = 0; agent < 2; agent++) {

						if (global.field[next_point[agent].y][next_point[agent].x] <= 0) {
							skipFlag = true;
							break;
						}

						bool wasMoved = now_state.my_area[convXY(next_point[agent])];
						bool isEnemyArea = now_state.enemy_area[convXY(next_point[agent])];

						if (isEnemyArea) {
							next_state.x[agent] = now_state.x[agent];
							next_state.y[agent] = now_state.y[agent];
							next_state.enemy_area.reset(convXY(next_point[agent]));

							next_state.score += (global.field[next_point[agent].y][next_point[agent].x] * (1 + ((search_depth - i) * fast_bonus_ratio))) * enemy_peel_bonus;
							//next_state.enemy_area_point = calcAreaScore(global.field, next_state.enemy_area);
						}
						else if (!wasMoved) {
							next_state.x[agent] = next_point[agent].x;
							next_state.y[agent] = next_point[agent].y;
							//evaluate1
							//next_state.score += (dxy[agent == 0 ? dxy1 : dxy2] != 0 && dxy[(agent == 0 ? dxy1 : dxy2) + 1] != 0) * diagonal_bonus;

							next_state.score += global.field[next_point[agent].y][next_point[agent].x] * (1 + ((search_depth - i) * fast_bonus_ratio));
							next_state.my_area.set(convXY(next_point[agent]));
							//next_state.my_area_point = calcAreaScore(global.field, next_state.my_area);
						}
						else {
							skipFlag = true;
							break;
						}
					}
					if (!skipFlag)
						nextBeamBucket << next_state;
				}
			}
		}
		//最終ターンにエリアポイントを評価に入れます。
		if (i == search_depth - 1)
			for (int k = 0; k < nowBeamBucket.size(); k++) {
				first_state.score += last_turn_bonus * (calcAreaScore(global.field, nowBeamBucket[i].my_area) - calcScore(global.field, nowBeamBucket[i].enemy_area));
			}
		//若干運ゲーな変更
		//sort
		std::sort(nextBeamBucket.begin(), nextBeamBucket.end(), [](const InfoBeamSearch& left, const InfoBeamSearch& right) {return left.score > right.score; });
		//erase
		{
			std::bitset<144> my_area = nextBeamBucket[0].my_area, enemy_log = nextBeamBucket[0].enemy_area;
			for (int k = 1; k < nextBeamBucket.size(); k++) {
				if (nextBeamBucket[k].my_area == my_area && nextBeamBucket[k].enemy_area == enemy_log) {
					nextBeamBucket[k].score += same_demerit;
				}
				else {
					my_area = nextBeamBucket[k].my_area, enemy_log = nextBeamBucket[k].enemy_area;
				}
			}

		}
		std::sort(nextBeamBucket.begin(), nextBeamBucket.end(), [](const InfoBeamSearch& left, const InfoBeamSearch& right) {return left.score > right.score; });
		//evaluate2
		{
			std::unordered_map<ULL, bool> um;
			for (int k = 0; k < nextBeamBucket.size(); k++) {
				ULL hash = nextBeamBucket[k].x[0] << 12 |
					nextBeamBucket[k].y[0] << 8 |
					nextBeamBucket[k].x[1] << 4 |
					nextBeamBucket[k].y[1];
				if (um.count(hash)) {
					nextBeamBucket[k].score += same_demerit;
				}
				else {
					um.insert({ hash, true });
				}
			}
		}
		//sort
		std::sort(nextBeamBucket.begin(), nextBeamBucket.end(), [](const InfoBeamSearch& left, const InfoBeamSearch& right) {return left.score > right.score; });
		//push nowBeamBucket
		nowBeamBucket.clear();
		for (int k = 0; k < std::min(nextBeamBucket.size(), beam_size); k++) {
			nowBeamBucket << std::move(nextBeamBucket[k]);
		}
		nextBeamBucket.clear();
	}

	//aggregate
	if (nowBeamBucket.size() == 0) {
		return 0.0;
	}

	return nowBeamBucket[0].score;
}

double Procon29::Computer::Search::evaluateConfirmScore(const InfoBeamSearch & info, const GlobalAlphaBeta & global, const Point enemyPos[2])
{
	//幅優先探索により今後ひっくり返るか計算する

	const int search_depth = global.search_depth;

	int fieldSizeX = global.board_size_x;
	int fieldSizeY = global.board_size_y;

	//char is 8bit and 1 byte.
#define XY_TO_CHAR(x,y) ((((x) & 0xF) << 4) | ((y) & 0xF))
#define CHAR_TO_X(c) (((c) >> 4) & 0xF)
#define CHAR_TO_Y(c) ((c) & 0xF)
	int result = 0;
	
	{
		for (int agent = 0; agent < MaxAgentNum; agent++) {
			auto field = info.enemy_area;

			int visit[188] = {};

			std::queue<std::pair<BYTE, BYTE>> q;

			q.push({ XY_TO_CHAR(info.x[agent], info.y[agent]), 1 });

			while (!q.empty()) {

				auto now = q.front();
				q.pop();

				const char & x = CHAR_TO_X(now.first);
				const char & y = CHAR_TO_Y(now.first);

				//unsignedだから0以下にはならない
				//		if (0 <= CHAR_TO_Y(now) && CHAR_TO_Y(now) < fieldSizeY && 0 <= CHAR_TO_X(now) && CHAR_TO_X(now) < fieldSizeX) {
				if (y < fieldSizeY && x < fieldSizeX && (visit[now.first] > now.second || visit[now.first] == 0)) {
					visit[now.first] = now.second;

					if (field[x + y * 12]) {
						q.push({ XY_TO_CHAR(x - 1, y),now.second + 2 });
						q.push({ XY_TO_CHAR(x, y + 1),now.second + 2 });
						q.push({ XY_TO_CHAR(x + 1, y),now.second + 2 });
						q.push({ XY_TO_CHAR(x, y - 1),now.second + 2 });
					}
					else {
						q.push({ XY_TO_CHAR(x - 1, y),now.second + 1 });
						q.push({ XY_TO_CHAR(x, y + 1),now.second + 1 });
						q.push({ XY_TO_CHAR(x + 1, y),now.second + 1 });
						q.push({ XY_TO_CHAR(x, y - 1),now.second + 1 });
					}

				}
			}

			int tmp = 0;

			for (auto y : step(fieldSizeY)) {
				for (auto x : step(fieldSizeX)) {
					if (y < fieldSizeY && x < fieldSizeX && field[x + y * 12] && visit[XY_TO_CHAR(x, y)] > search_depth) {
						tmp += global.field[y][x];
					}
				}
			}

			result += tmp;
		}
	}

	{
		for (int agent = 0; agent < MaxAgentNum; agent++) {
			auto field = info.my_area;

			int visit[188] = {};

			std::queue<std::pair<BYTE, BYTE>> q;

			q.push({ XY_TO_CHAR(info.x[agent], info.y[agent]), 1 });

			while (!q.empty()) {

				auto now = q.front();
				q.pop();

				const char & x = CHAR_TO_X(now.first);
				const char & y = CHAR_TO_Y(now.first);

				//unsignedだから0以下にはならない
				//		if (0 <= CHAR_TO_Y(now) && CHAR_TO_Y(now) < fieldSizeY && 0 <= CHAR_TO_X(now) && CHAR_TO_X(now) < fieldSizeX) {
				if (y < fieldSizeY && x < fieldSizeX && (visit[now.first] > now.second || visit[now.first] == 0)) {
					visit[now.first] = now.second;

					if (field[x + y * 12]) {
						q.push({ XY_TO_CHAR(x - 1, y),now.second + 2 });
						q.push({ XY_TO_CHAR(x, y + 1),now.second + 2 });
						q.push({ XY_TO_CHAR(x + 1, y),now.second + 2 });
						q.push({ XY_TO_CHAR(x, y - 1),now.second + 2 });
					}
					else {
						q.push({ XY_TO_CHAR(x - 1, y),now.second + 1 });
						q.push({ XY_TO_CHAR(x, y + 1),now.second + 1 });
						q.push({ XY_TO_CHAR(x + 1, y),now.second + 1 });
						q.push({ XY_TO_CHAR(x, y - 1),now.second + 1 });
					}

				}
			}

			int tmp = 0;

			for (auto y : step(fieldSizeY)) {
				for (auto x : step(fieldSizeX)) {
					if (y < fieldSizeY && x < fieldSizeX && field[x + y * 12] && visit[XY_TO_CHAR(x, y)] > search_depth) {
						tmp += global.field[y][x];
					}
				}
			}

			result -= tmp;
		}
	}

	return result;
}
