#pragma once

#include "Procon29.hpp"
#include "Team.hpp"
#include <bitset>

namespace Procon29 {

	class Computer {
	public:
		//Move Peel選択できるようにする。
		//返り値：Vec3,x:x位置,y:y位置,z:MoveOrPeel 
		//z,0:stay 1:move 2:peel
		using ResultType = Array<std::tuple<Array<Vec3>, double>>;
	private:
		class Evaluate {
		private:
			const int evaluateType = 0;
			double evaluatePercent = 0.1;
		public:
			int game_turn;
			bool lock;
			int beam_width;
			bool useStupid;
			Array<Point> point_history[2];
			Point before_calc_point[2];
			TeamType my_team;
			Evaluate();
			Evaluate(const int width,const TeamType teamtype,const bool useStupid);
		};

		class Search {
		private:
			//const int searchType = 0;

			using BYTE = char;
			using ULL = unsigned long long int;

			struct InfoMoveOnly {
				BYTE first_dir[2];
				BYTE action[2];
				std::bitset<144> my_area;
				BYTE x[2], y[2];
				//BYTE hash;
				double score;
			};

			struct InfoBeamSearch {
				BYTE first_dir[2];
				BYTE action[2];
				std::bitset<144> my_area;
				std::bitset<144> enemy_area;
				BYTE x[2], y[2];
				//BYTE hash;
				double score;
				double my_area_point;
				double enemy_area_point;
			};

			struct InfoStupid {
				std::bitset<144> my_area;
				std::bitset<144> enemy_area;
				BYTE x[2], y[2];
				BYTE action[2];
				BYTE enemy_x[2], enemy_y[2];
			};

			struct InfoAlphaBeta {
				BYTE first_dir[2];
				BYTE action[2];
				std::bitset<144> my_area;
				std::bitset<144> enemy_area;
				BYTE my_x[2], my_y[2];
				BYTE enemy_x[2], enemy_y[2];
				//BYTE hash;
				double my_area_point;
				double enemy_area_point;
				//general
				int depth;
			};

			struct ResultAlphaBeta {
				BYTE first_dir[2];
				BYTE action[2];
				double score;
				ResultAlphaBeta();
			};

			struct GlobalAlphaBeta {
				//null == -32
				BYTE field[12][12];
				int board_size_x, board_size_y;

				double alpha;
				double beta;

				int game_turn;
				int search_depth;
			};


		public:
			int calcScore(const Field & field, const std::bitset<144>& my_area) const;
			int calcAreaScore(const Field & field, const std::bitset<144>& my_area) const;
			int calcTileScore(const Field & field, const std::bitset<144>& my_area) const;
			int calcScore(const BYTE field[12][12], const std::bitset<144>& my_area) const;
			int calcAreaScore(const BYTE field[12][12], const std::bitset<144>& my_area) const;
			int calcTileScore(const BYTE field[12][12], const std::bitset<144>& my_area) const;

			//結局本選で使ったのはこれだけ
			ResultType beamSearch(const Teams& teams, const Field& field, const Evaluate& evaluate);
			
			//以下基本使わない

			//ResultType searchMoveOnly(const Teams& teams, const Field& field, const Evaluate& evaluate);
			//間抜けなAIを狩るための関数1ターン全手読み
			//愚直に来ると予測して逆に狩る
			ResultType searchStupid(const Teams& teams, const Field& field, const Evaluate& evaluate);
			//同時完全探索2ターン読み
			ResultType searchPerfect(const Teams& teams, const Field& field, const Evaluate& evaluate);
			ResultAlphaBeta funcSearchPerfect(const InfoAlphaBeta& info, GlobalAlphaBeta& global);
			//alpha beta 探索　2ターン読み
			ResultType searchAlphaBeta(const Teams& teams, const Field& field, const Evaluate& evaluate);
			ResultAlphaBeta enemyTeamAlphaBeta(const InfoAlphaBeta& info, GlobalAlphaBeta& global);
			ResultAlphaBeta enemyTeamAlphaBetaAsync(const InfoAlphaBeta& info, GlobalAlphaBeta& global);
			ResultAlphaBeta myTeamAlphaBeta(const InfoAlphaBeta& info, GlobalAlphaBeta& global);

			double evaluateBeamSearch(const InfoBeamSearch& info,const GlobalAlphaBeta& global,const Point enemyPos[2]);
			
			//確定石計算
			double evaluateConfirmScore(const InfoBeamSearch& info, const GlobalAlphaBeta& global, const Point enemyPos[2]);
		
		};
		
		Evaluate evaluate;
		Search search;
		std::future<ResultType> future;

	public:

		enum class  ComputerState {
			Null,
			Ready,
			Calc,
		};

		//計算を開始する
		void calculate(const Teams& teams,const Field& field,const int & game_turn);

		//現在の状態を取得する
		ComputerState getState() const;

		//getState == CoputerState::Ready　なら結果か例外を返す。
		//結果はおすすめ評価値ごとの移動方向。
		ResultType getResult();

		Computer();
		Computer(const int width,const TeamType teamtype,const bool useStupid);

	};

}