//注目、本選で使用した関数はこれだけです。
#include "Computer.hpp"

Procon29::Computer::ResultType Procon29::Computer::Search::beamSearch(const Teams& teams, const Field& field, const Evaluate& evaluate)
{
	const size_t beam_size = evaluate.beam_width;
	const size_t result_size = 3;
	const size_t my_team = evaluate.my_team == TeamType::MyTeam ? 0 : 1;
	const size_t enemy_team = evaluate.my_team == TeamType::EnemyTeam ? 0 : 1;
	const int search_depth = std::min(20, MaxTurn - (evaluate.game_turn));
	const int same_demerit = -30;
	const int was_moved_demerit = -5;
	const int wait_demerit = -10;
	const double diagonal_bonus = 1.5;
	const double fast_bonus = 1.03;
	const double enemy_peel_bonus = 0.9;
	const double my_area_merit = 0.4;
	const double enemy_area_merit = 0.8;
	const int minus_demerit = -2;
	const bool isConfirmStone = true;

	int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < field.boardSizeGetter().x && p.y < field.boardSizeGetter().y);
	};

	auto convXY = [](const Point& p) {
		return p.x + p.y * 12;
	};

	if (evaluate.useStupid && search_depth <= 1) {
		return this->searchPerfect(teams, field, evaluate);
	}

	Array<InfoBeamSearch> nowBeamBucket;
	nowBeamBucket.reserve(beam_size);
	Array<InfoBeamSearch> nextBeamBucket;
	nowBeamBucket.reserve(beam_size * 100);

	InfoBeamSearch first_state;
	first_state.first_dir[0] = -1;
	first_state.first_dir[1] = -1;
	first_state.x[0] = teams[my_team].agentsGeter()[0].nowPoint.x;
	first_state.y[0] = teams[my_team].agentsGeter()[0].nowPoint.y;
	first_state.x[1] = teams[my_team].agentsGeter()[1].nowPoint.x;
	first_state.y[1] = teams[my_team].agentsGeter()[1].nowPoint.y;
	for (auto i : step(field.boardSizeGetter())) {
		if (field.boardGetter().at(i).teamType != TeamType::None && field.boardGetter().at(i).teamType != evaluate.my_team) {
			first_state.enemy_area.set(convXY(i));
		}
		if (field.boardGetter().at(i).teamType == evaluate.my_team) {
			first_state.my_area.set(convXY(i));
		}
	}

	first_state.score = 0;
	first_state.my_area_point = calcAreaScore(field, first_state.my_area);
	first_state.enemy_area_point = calcAreaScore(field, first_state.enemy_area);

	nowBeamBucket << first_state;

	for (int i = 0; i < search_depth; i++) {
		//enumlate
		for (int k = 0; k < nowBeamBucket.size(); k++) {
			auto& now_state = nowBeamBucket[k];
			for (int dxy1 = 0; dxy1 < 9; dxy1++) {
				for (int dxy2 = 0; dxy2 < 9; dxy2++) {

					Point next_point[2] = {
						Point(now_state.x[0] + dxy[dxy1], now_state.y[0] + dxy[dxy1 + 1]),
						Point(now_state.x[1] + dxy[dxy2], now_state.y[1] + dxy[dxy2 + 1]) };

					if (next_point[0] == next_point[1])
						continue;

					if (next_point[0] == teams[enemy_team].agentsGeter()[0].nowPoint ||
						next_point[0] == teams[enemy_team].agentsGeter()[1].nowPoint ||
						next_point[1] == teams[enemy_team].agentsGeter()[0].nowPoint ||
						next_point[1] == teams[enemy_team].agentsGeter()[1].nowPoint)
						continue;

					if (!checkIn(next_point[0]) ||
						!checkIn(next_point[1]))
						continue;

					InfoBeamSearch next_state;
					next_state.first_dir[0] = now_state.first_dir[0];
					next_state.first_dir[1] = now_state.first_dir[1];
					if (now_state.first_dir[0] == -1 && now_state.first_dir[1] == -1) {
						if (evaluate.lock) {
							if (evaluate.before_calc_point[0] == next_point[0] &&
								evaluate.before_calc_point[1] == next_point[1])
								continue;
						}
						next_state.first_dir[0] = dxy1;
						next_state.first_dir[1] = dxy2;

						for (int agent = 0; agent < 2; agent++) {
							//bool wasMoved = now_state.my_area[convXY(next_point[agent])];
							bool isEnemyArea = now_state.enemy_area[convXY(next_point[agent])];

							if (isEnemyArea)
								next_state.action[agent] = 2;
							else
								next_state.action[agent] = 1;


						}
					}
					else if (now_state.first_dir[0] == -1 || now_state.first_dir[1] == -1) {
						return ResultType();
					}
					next_state.score = now_state.score;
					next_state.my_area = now_state.my_area;
					next_state.enemy_area = now_state.enemy_area;
					next_state.my_area_point = now_state.my_area_point;
					next_state.enemy_area_point = now_state.enemy_area_point;

					for (int agent = 0; agent < 2; agent++) {

						bool wasMoved = now_state.my_area[convXY(next_point[agent])];

						bool isEnemyArea = now_state.enemy_area[convXY(next_point[agent])];

						if (isEnemyArea) {
							next_state.x[agent] = now_state.x[agent];
							next_state.y[agent] = now_state.y[agent];
							next_state.enemy_area.reset(convXY(next_point[agent]));
							if (field.boardGetter().at(next_point[agent]).score <= 0)
								next_state.score += (field.boardGetter().at(next_point[agent]).score * pow(fast_bonus, search_depth - i) + minus_demerit) * enemy_peel_bonus;
							else
								next_state.score += (field.boardGetter().at(next_point[agent]).score * pow(fast_bonus, search_depth - i)) * enemy_peel_bonus;
							next_state.enemy_area_point = calcAreaScore(field, next_state.enemy_area);
						}
						else {
							next_state.x[agent] = next_point[agent].x;
							next_state.y[agent] = next_point[agent].y;
							if (!wasMoved) {
								//evaluate1
								next_state.score += (dxy[agent == 0 ? dxy1 : dxy2] != 0 && dxy[(agent == 0 ? dxy1 : dxy2) + 1] != 0) * diagonal_bonus;
								if (field.boardGetter().at(next_point[agent]).score <= 0)
									next_state.score += (field.boardGetter().at(next_point[agent]).score + minus_demerit) * pow(fast_bonus, search_depth - i);
								else
									next_state.score += field.boardGetter().at(next_point[agent]).score * pow(fast_bonus, search_depth - i);
								next_state.my_area.set(convXY(next_point[agent]));
								next_state.my_area_point = calcAreaScore(field, next_state.my_area);
							}
							else {
								//evaluate1
								if ((agent == 0 ? dxy1 : dxy2) == 6)
									next_state.score += wait_demerit * pow(fast_bonus, search_depth - i);
								else
									next_state.score += was_moved_demerit * pow(fast_bonus, search_depth - i);
							}
						}
					}

					next_state.score += (next_state.my_area_point - now_state.my_area_point) * my_area_merit +
						(now_state.enemy_area_point - next_state.enemy_area_point) * enemy_area_merit * pow(fast_bonus, search_depth - i);
					if (isConfirmStone) {

					}
					nextBeamBucket << next_state;
				}
			}
		}
		//若干運ゲーな変更
		//sort
		std::sort(nextBeamBucket.begin(), nextBeamBucket.end(), [](const InfoBeamSearch& left, const InfoBeamSearch& right) {return left.score > right.score; });
		//erase
		{
			std::bitset<144> my_area = nextBeamBucket[0].my_area, enemy_log = nextBeamBucket[0].enemy_area;
			for (int k = 1; k < nextBeamBucket.size(); k++) {
				if (nextBeamBucket[k].my_area == my_area && nextBeamBucket[k].enemy_area == enemy_log) {
					nextBeamBucket[k].score += same_demerit;
				}
				else {
					my_area = nextBeamBucket[k].my_area, enemy_log = nextBeamBucket[k].enemy_area;
				}
			}

		}
		std::sort(nextBeamBucket.begin(), nextBeamBucket.end(), [](const InfoBeamSearch& left, const InfoBeamSearch& right) {return left.score > right.score; });
		//evaluate2
		{
			std::unordered_map<ULL, bool> um;
			for (int k = 0; k < nextBeamBucket.size(); k++) {
				ULL hash = nextBeamBucket[k].x[0] << 12 |
					nextBeamBucket[k].y[0] << 8 |
					nextBeamBucket[k].x[1] << 4 |
					nextBeamBucket[k].y[1];
				if (um.count(hash)) {
					nextBeamBucket[k].score += same_demerit;
				}
				else {
					um.insert({ hash, true });
				}
			}
		}
		//sort
		std::sort(nextBeamBucket.begin(), nextBeamBucket.end(), [](const InfoBeamSearch& left, const InfoBeamSearch& right) {return left.score > right.score; });
		//push nowBeamBucket
		nowBeamBucket.clear();
		for (int k = 0; k < std::min(nextBeamBucket.size(), beam_size); k++) {
			nowBeamBucket << std::move(nextBeamBucket[k]);
		}
		nextBeamBucket.clear();
	}

	//aggregate
	if (nowBeamBucket.size() == 0) {
		return ResultType();
	}
	ResultType result;
	for (int i = 0; i < std::min(nowBeamBucket.size(), result_size); i++) {
		const auto & now_state = nowBeamBucket[i];
		Array<Vec3> first_move_point = { Vec3(teams[my_team].agentsGeter()[0].nowPoint + Point(dxy[now_state.first_dir[0]],dxy[now_state.first_dir[0] + 1]),now_state.action[0]),
			Vec3(teams[my_team].agentsGeter()[1].nowPoint + Point(dxy[now_state.first_dir[1]],dxy[now_state.first_dir[1] + 1]),now_state.action[1]) };
		bool same = false;
		for (int k = 0; k < result.size(); k++)
		{
			bool check = true;
			for (int m = 0; m < std::get<Array<Vec3>>(result[k]).size(); m++) {
				if (std::get<Array<Vec3>>(result[k])[m] != first_move_point[m]) {
					check = false;
				}
			}
			if (check) {
				same = true;
			}
		}
		if (same) {
			continue;
		}
		std::tuple<Array<Vec3>, double> candidate = {
			first_move_point,
			now_state.score / nowBeamBucket[0].score };
		result << candidate;
	}

	return result;
}