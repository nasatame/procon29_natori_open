#pragma once

#include "Procon29.hpp"

namespace Procon29 {

	struct Agent {
		Point nowPoint;
		Point nextPoint;
		bool selectedNextPoint;
		bool nowSelect;
		Rect m_agentRect{ TileSize };
		int32 action;
		//0:stay 1:move 2:peel

		bool isMove(/*����*/) const;
		void draw(TeamType team, int number) const;
		void update(Point pos);
		void moveing(/*����*/);

		Agent(String initilizeString);
		Agent(Point initilizePoint);
		Agent() = delete;

		[[nodiscard]] String format() const;
	};

}
