#include "Field.hpp"

bool Procon29::Field::load(String fileName)
{
	TextReader reader(fileName);
	if (reader.isOpened()) {
		String input = reader.readAll();
		if (input.size() < 16) {
			return false;
		}
		Array<String> singleSplit = input.split(':');
		Array<Array<String>> splitted;
		for (auto str : singleSplit) {
			splitted << str.split(' ').removed(U"");
		}
		m_boardSize = Size(Parse<int32>(splitted.at(0).at(1)), Parse<int32>(splitted.at(0).at(0)));
		if (m_board.size() != Size(12, 12)) {
			m_board = TileGrid(12,12);
		}
		for (auto point : step(m_boardSize)) {
			m_board.at(point).score = Parse<int32>(splitted.at(point.y + 1).at(point.x));
			m_board.at(point).teamType = TeamType::None;
			m_board.at(point).exist = true;


		}
		m_board.at(Point(Parse<int32>(splitted.at(m_boardSize.y + 1).at(1)) - 1, Parse<int32>(splitted.at(m_boardSize.y + 1).at(0)) - 1)).teamType = TeamType::MyTeam;
		m_board.at(Point(Parse<int32>(splitted.at(m_boardSize.y + 2).at(1)) - 1, Parse<int32>(splitted.at(m_boardSize.y + 2).at(0)) - 1)).teamType = TeamType::MyTeam;
		m_board.at(Point(Parse<int32>(splitted.at(m_boardSize.y + 3).at(1)) - 1, Parse<int32>(splitted.at(m_boardSize.y + 3).at(0)) - 1)).teamType = TeamType::EnemyTeam;
		m_board.at(Point(Parse<int32>(splitted.at(m_boardSize.y + 4).at(1)) - 1, Parse<int32>(splitted.at(m_boardSize.y + 4).at(0)) - 1)).teamType = TeamType::EnemyTeam;
		return true;
	}
	return false;
}

const Procon29::TileGrid & Procon29::Field::boardGetter() const
{
	return m_board;
}

void Procon29::Field::draw() const
{
	const Color enemyTeamColor = !isColorReverse ? Palette::Indianred : Palette::Lightblue;
	const Color myTeamColor = !isColorReverse ? Palette::Lightblue : Palette::Indianred;

	for (auto tile : step(m_board.size())) {
		//tile.x : xpos tile.y : ypos
		//m_board.at(tile). :acces
		if (m_board.at(tile).teamType == Procon29::TeamType::EnemyTeam) {
			m_tileRect.movedBy(TileSize * tile).draw(enemyTeamColor);
		}
		else if (m_board.at(tile).teamType == Procon29::TeamType::MyTeam) {
			m_tileRect.movedBy(TileSize * tile).draw(myTeamColor);
		}
		else if (m_board.at(tile).teamType == Procon29::TeamType::None) {
			m_tileRect.movedBy(TileSize * tile).draw(Palette::Lightslategray);
		}
		else if (m_board.at(tile).teamType == Procon29::TeamType::Null) {
			m_tileRect.movedBy(TileSize * tile).draw(Palette::Darkcyan);
			continue;
		}

		//�I������Ă�����
		if (m_board.at(tile).selecting)
			m_tileRect.movedBy(TileSize * tile).drawFrame(4, 0, ColorF(Palette::Lightgreen) * 0.77);
		FontAsset(U"Fieldfont")(m_board.at(tile).score).draw(TileSize * tile.x + 1, TileSize * tile.y + 1, Palette::Yellow);

		m_tileRect.movedBy(TileSize * tile).drawFrame(1, 0, Palette::Black);
	}
}

void Procon29::Field::update(bool colorFlag, bool superUserFlag)
{
	isColorReverse = colorFlag;
	isSuperUser = superUserFlag;

	if (isSuperUser) {
		if (MouseL.down()) {
			Point selectPoint = posConverter(Cursor::Pos());
			if (0 <= selectPoint.x && 0 <= selectPoint.y && m_boardSize.x > selectPoint.x && m_boardSize.y > selectPoint.y) {
				if (m_board.at(selectPoint).teamType == TeamType::MyTeam) {
					m_board.at(selectPoint).teamType = TeamType::EnemyTeam;
				}
				else if (m_board.at(selectPoint).teamType == TeamType::EnemyTeam) {
					m_board.at(selectPoint).teamType = TeamType::None;
				}
				else if (m_board.at(selectPoint).teamType == TeamType::None) {
					m_board.at(selectPoint).teamType = TeamType::MyTeam;
				}
			}
		}

	}
}

void Procon29::Field::lightUp(Point pos)
{
	selectReset();
	if (pos.y > 0 && m_board.at(pos.y - 1, pos.x).exist) {
		m_board.at(pos.y - 1, pos.x).selecting = true;
	}
	if (MaxFieldY - 1 > pos.y && m_board.at(pos.y + 1, pos.x).exist) {
		m_board.at(pos.y + 1, pos.x).selecting = true;
	}
	if (pos.x > 0 && m_board.at(pos.y, pos.x - 1).exist) {
		m_board.at(pos.y, pos.x - 1).selecting = true;
	}
	if (MaxFieldX - 1 > pos.x &&m_board.at(pos.y, pos.x + 1).exist) {
		m_board.at(pos.y, pos.x + 1).selecting = true;
	}
	if (pos.y > 0 && pos.x > 0 && m_board.at(pos.y - 1, pos.x - 1).exist) {
		m_board.at(pos.y - 1, pos.x - 1).selecting = true;
	}
	if (MaxFieldY - 1 > pos.y &&pos.x > 0 && m_board.at(pos.y + 1, pos.x - 1).exist) {
		m_board.at(pos.y + 1, pos.x - 1).selecting = true;
	}
	if (pos.y > 0 && MaxFieldX - 1 > pos.x && m_board.at(pos.y - 1, pos.x + 1).exist) {
		m_board.at(pos.y - 1, pos.x + 1).selecting = true;
	}
	if (MaxFieldY - 1 > pos.y &&MaxFieldX - 1 > pos.x &&m_board.at(pos.y + 1, pos.x + 1).exist) {
		m_board.at(pos.y + 1, pos.x + 1).selecting = true;
	}
}

void Procon29::Field::fieldUpdate(Array<Point> pos, Array<int32> action)
{
	//�����D��
	for (int i = 0; i < 4; i++) {
		if (action[i] != 0) {
			if (action[i] == 0 || action[i] == 1) {
				m_board.at(pos[i]).teamType = (i < 2 ? Procon29::TeamType::MyTeam : Procon29::TeamType::EnemyTeam);
			}
			else if (action[i] == 2) {
				m_board.at(pos[i]).teamType = Procon29::TeamType::None;
			}
		}
	}
}

bool Procon29::Field::selectingField(Point pos) const
{
	if (pos.x >= MaxFieldX || pos.y >= MaxFieldY || pos.x < 0 || pos.y < 0) {
		return false;
	}
	return (m_board.at(pos).selecting);
}

void Procon29::Field::selectReset()
{
	for (auto tile : step(m_board.size())) {
		m_board.at(tile).selecting = false;
	}
}

Point Procon29::Field::posConverter(Point pos) {
	return { pos.x / TileSize,pos.y / TileSize };
}

Size Procon29::Field::boardSizeGetter() const
{
	return this->m_boardSize;
}

void Procon29::Field::set(Point changePoint, Procon29::TeamType changeType)
{
	m_board.at(changePoint).teamType = changeType;
}

Array<Point> Procon29::Field::getSurroundArea(const TeamType& teamType)
{
	const auto& field = *this;

	Array<Array<bool>> visit(MaxFieldY, Array<bool>(MaxFieldX, false));

	Array<Point> q;

	int fieldSizeX = 12;
	int fieldSizeY = 12;

	for (auto y : step(MaxFieldY)) {
		if (field.boardGetter().at(y, 0).exist == false) {
			fieldSizeY = y + 1;
			break;
		}
	}
	for (auto x : step(MaxFieldX)) {
		if (field.boardGetter().at(0, x).exist == false) {
			fieldSizeX = x + 1;
			break;
		}
	}
	for (auto y : step(fieldSizeY)) {
		q.push_back({ 0, y });
		q.push_back({ fieldSizeX - 1,y });
	}
	for (auto x : step(fieldSizeX)) {
		q.push_back({ x , 0 });
		q.push_back({ x , fieldSizeY - 1 });
	}

	while (!q.isEmpty()) {

		auto now = q.front();
		q.pop_front();

		if (0 <= now.y && now.y < fieldSizeY && 0 <= now.x && now.x < fieldSizeX) {

			if (visit.at(now.y).at(now.x))
				continue;
			visit.at(now.y).at(now.x) = true;

			if (field.boardGetter().at(now.y, now.x).teamType != teamType) {

				q.push_back({ now.x - 1	,now.y });
				q.push_back({ now.x		,now.y + 1 });
				q.push_back({ now.x + 1 ,now.y });
				q.push_back({ now.x		,now.y - 1 });

			}

		}

	}

	s3d::Array<Point> result = {};

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			if (teamType != field.boardGetter().at(y, x).teamType && visit.at(y).at(x) == false) {
				result << Point(x,y);
			}
		}
	}

	return result;
}


String Procon29::Field::format() const
{
	String ret;
	ret += Format(m_boardSize.x, U",", m_boardSize.y) + U":";
	for (int y = 0; y < m_boardSize.y; y++) {
		for (int x = 0; x < m_boardSize.x; x++) {
			ret += m_board.at(y, x).format() + U",";
		}
		ret += U":";
	}
	return ret;
}

bool Procon29::Field::canPeel(Point pos, Procon29::TeamType teamType)
{
	if (m_board.at(pos).teamType == teamType) {
		return true;
	}
	return false;
}

Procon29::TeamType Procon29::Field::teamTypeGetter(Point pos) const
{
	Procon29::TeamType tileColor = m_board[pos.y][pos.x].teamTypeGetter();
	return tileColor;
}

Procon29::Field::Field(String initilizeString, bool isInvert)
{
	Array<String> singleSplit = initilizeString.split(':');
	Array<Array<String>> splitted;
	for (auto str : singleSplit) {
		splitted << str.split(',').removed(U"");
	}
	m_boardSize = Size(Parse<int32>(splitted.at(0).at(0)), Parse<int32>(splitted.at(0).at(1)));

	for (auto point : step(m_boardSize)) {
		m_board.at(point) = Tile(splitted.at(point.y + 1).at(point.x), isInvert);
	}
}

Procon29::Field::Field(String fileName)
{
	bool loadResult = load(fileName);
	assert(loadResult);
}

void Procon29::Field::rotate90()
{
	//Field reverse
	TileGrid tileGrid(12, 12);
	for (auto i : step(boardSizeGetter())) {
		tileGrid.at({ i.y, boardSizeGetter().x - i.x - 1 }) = boardGetter().at(i);
	}

	m_boardSize= Size{ boardSizeGetter().y,boardSizeGetter().x };
	this->m_board = tileGrid;

}


