#include "SaveData.hpp"

void Procon29::SaveData::save(Teams saveTeams, Field saveField, Computer::ResultType saveResult)
{
	teams = saveTeams;
	field = saveField;
	result = saveResult;
	return;
}

void Procon29::SaveData::load(Teams& loadTeams, Field& loadField, Computer::ResultType& loadResult)
{
	loadTeams = teams;
	loadField = field;
	loadResult = result;
	return;
}
