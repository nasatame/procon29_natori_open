#pragma once

#include "Procon29.hpp"
#include "Field.hpp"
#include "Team.hpp"

namespace Procon29 {

	enum class Action {
		Stay,
		Move,
		Peel
	};

	class SenjuKaneko {
	private:
		bool colorReverse;
		//bool placeReverse;
		size_t positionIndex;
		//左腕(850,650) 右腕(1250,650)
		Image kaneko;
		//move stay peel
		//center 1200,350
		const double magnification = 0.2;
		Array<Point> correction;
		Array<Texture> blackMoveTex;
		Array<Texture> blackPeelTex;
		Array<Texture> blackStayTex;
		Array<Texture> redMoveTex;
		Array<Texture> redPeelTex;
		Array<Texture> redStayTex;
		Texture drawnTexture;

		//team info
		Action blackAction;
		Action redAction;
		int blackDist;
		int redDist;

	public:
		void update(Teams& teams, Field& field);
		void draw() const;

		SenjuKaneko();

		//チームとフィールドを90度回転する。
		void rotate90(Teams& teams,Field& field);
	};

}