﻿# include "Procon29.hpp"
# include "GameManager.hpp"
# include "Computer.hpp"
#include "SenjuKaneko.hpp"

//F7:スーパーユーザ
//F9:色反転
void Main()
{

	Window::Resize(Procon29::WindowSize);
	Graphics::SetBackground(ColorF(0.8, 0.9, 1.0));

	Procon29::LoadData();

	//ゲーム用変数の定義・初期化

	Procon29::GameManager gameManager;

	Procon29::Teams teams;
	Procon29::Field field;
	Procon29::SaveData saveData;
	Procon29::Computer computer(500, Procon29::TeamType::MyTeam, true);//random
	Procon29::Computer sub_computer(500, Procon29::TeamType::EnemyTeam, false);//random
	Procon29::Computer::ResultType calculateResults;
	Procon29::Computer::ResultType calculateSubResults;
	bool isResultAcquired = false;
	bool isSubResultAcquired = true;

	Procon29::SenjuKaneko senjuKaneko;

	gameManager.setting(teams, field);
	saveData.save(teams, field, calculateResults);

	if (Procon29::VSComputer || Procon29::ComputerVSComputer) {
		computer.calculate(teams, field, gameManager.turnGetter());
		if (Procon29::ComputerVSComputer) {
			sub_computer.calculate(teams, field, gameManager.turnGetter());
			isSubResultAcquired = false;
		}
	}

	TextReader tr(gameManager.fileName);
	Point selectingPoint = { -1,-1 };
	Point selectingAgentPoint = { -1,-1 };
	Rect coverRect(Procon29::TileSize / 2);
	ColorF coverColor = ColorF(Palette::Blue).setA(0.5);
	ColorF sub_coverColor = ColorF(Palette::Red).setA(0.5);

	//field.setter();

	bool isSuperUserMode = false;
	bool isColorReverse = false;

	while (System::Update()) {
		Window::Centering();

		if (!isSuperUserMode && Procon29::MaxTurn <= gameManager.turnGetter()) {

			//Draw
			field.draw();
			teams[0].draw();
			teams[1].draw();
			FontAsset(U"Fieldfont")(U"時間経過 : ", gameManager.timerGetter()).draw(12 * 64 + 10, 160, Palette::Black);
			//FontAsset(U"Fieldfont")(selectingPoint).draw(12 * 64 + 10, 210, Palette::Black);
			FontAsset(U"Fieldfont")(U"ターン数 : ", gameManager.turnGetter()).draw(12 * 64 + 10, 190, Palette::Black);

			Window::ClientRect().draw(Color(Palette::Gray, 100));

			const Procon29::TeamType win_team = gameManager.judgeWinner(teams);
			assert(win_team != Procon29::TeamType::Null);
			const int32 win_team_num = (win_team == Procon29::TeamType::None ? 0 : (win_team == Procon29::TeamType::MyTeam ? 1 : 2));
			const String team_name[] = { String(U"Draw"),String(U"MyTeamWin"),String(U"EnemyTeamWin") };

			FontAsset(U"TurnInfomation")(U"GAME END : ", team_name[win_team_num]).drawAt(Window::Center() / Point(1, 2), Palette::Black);

			gameManager.drawScoreGraph();
			continue;
		}

		//Update
		//SuperUserMode
		if (KeyF7.down()) {
			isSuperUserMode = !isSuperUserMode;
			if (isSuperUserMode) {
				Graphics::SetBackground(Palette::Lightpink);
			}
			else {
				Graphics::SetBackground(ColorF(0.8, 0.9, 1.0));
			}
		}

		//色反転
		if (KeyF9.down()) {
			isColorReverse = !isColorReverse;
		}

		field.update(isColorReverse,isSuperUserMode);
		for(int i = 0; i < Procon29::MaxTeamNum; i++)
			teams[i].update(isColorReverse);

		if (isSuperUserMode) {
			if (KeyUp.down()) {
				gameManager.timerIncrement();
			}
			if (KeyDown.down()) {
				gameManager.timerDecrement();
			}
			//agents_SUPER_move_function
			if (MouseR.down()) {
				Array<Point> agentsPoint;
				Point selectedPoint = { -1,-1 };
				for (int i = 0; i < Procon29::MaxTeamNum; i++) {
					for (int j = 0; j < teams[i].agentsGeter().size(); j++) {
						agentsPoint << teams[i].agentsGeter().at(j).nowPoint;
						if (teams[i].agentsGeter().at(j).nowSelect) {
							selectedPoint = teams[i].agentsGeter().at(j).nowPoint;
						}
					}
				}

				Point selectPoint = field.posConverter(Cursor::Pos());
				if (0 <= selectPoint.x && 0 <= selectPoint.y && field.boardSizeGetter().x > selectPoint.x && field.boardSizeGetter().y > selectPoint.y) {
					if (agentsPoint.all([=](Point p) {
						return p != selectPoint; })) {
						if (selectedPoint != Point(-1, -1)) {
							teams[0].forcedAgentPointChange(selectPoint,isSuperUserMode);
							teams[1].forcedAgentPointChange(selectPoint,isSuperUserMode);
						}
					}else{
						teams[0].invalidation();
						teams[1].invalidation();
						teams[0].onMouseAgent(selectPoint);
						teams[1].onMouseAgent(selectPoint);
					}
				}
			}

			teams[0].forcedGiveRationakityToTheField(field, isSuperUserMode);
			teams[1].forcedGiveRationakityToTheField(field, isSuperUserMode);
			
			teams[0].culcScore(field);
			teams[1].culcScore(field);
		}
		else {


			if (gameManager.timerGetter().isRunning()) {
				//クリックを判定
				if (MouseL.down()) {
					//マウス位置 -> 12*12座標に変換
					selectingPoint = Cursor::Pos();
					selectingPoint = field.posConverter(selectingPoint);
					//選択マス優先
					//フィールド範囲内判定含む
					if (field.selectingField(selectingPoint)) {
						//黄色マスだった場合　全黄色マスに対してリセット
						field.selectReset();
						if (!Procon29::VSComputer) {
							teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
						}
						teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));

					}
					//エージェント上部だった場合
					if (!Procon29::VSComputer) {
						if (teams[0].onMouseAgent(selectingPoint)) {
							teams[1].invalidation();
							field.lightUp(selectingPoint);
						}
					}

					if (teams[1].onMouseAgent(selectingPoint)) {
						teams[0].invalidation();
						field.lightUp(selectingPoint);
					}

				}
				//Peel処理
				if (KeyP.down()) {
					if (!Procon29::VSComputer) {
						selectingAgentPoint = teams[0].nowSelectAgent();
						if (selectingAgentPoint.x != -1 && field.canPeel(selectingAgentPoint, teams[0].teamTypeGetter())) {
							teams[0].changePeel();
						}
					}
					selectingAgentPoint = teams[1].nowSelectAgent();
					if (selectingAgentPoint.x != -1 && field.canPeel(selectingAgentPoint, teams[1].teamTypeGetter())) {
						teams[1].changePeel();
					}

				}
				else if (KeyM.down()) {
					if (!Procon29::VSComputer) {
						selectingAgentPoint = teams[0].nowSelectAgent();
						if (selectingAgentPoint.x != -1 && field.canPeel(selectingAgentPoint, teams[0].teamTypeGetter())) {
							teams[0].changeMove();
						}
					}

					selectingAgentPoint = teams[1].nowSelectAgent();
					if (selectingAgentPoint.x != -1 && field.canPeel(selectingAgentPoint, teams[1].teamTypeGetter())) {
						teams[1].changeMove();
					}

				}

				//qwer : エージェント番号
				//z : skip  c : calc  b : plevier
				//num_key : 方向

				//agent選択
				if (KeyW.down()) {
					teams[0].invalidation();
					teams[1].invalidation();
					teams[0].keySelectAgents(0);
					selectingPoint = teams[0].agentsGeter()[0].nowPoint;
					field.lightUp(selectingPoint);
				}
				else if (KeyR.down()) {
					teams[0].invalidation();
					teams[1].invalidation();
					teams[0].keySelectAgents(1);
					selectingPoint = teams[0].agentsGeter()[1].nowPoint;
					field.lightUp(selectingPoint);
				}
				else if (Key2.down()) {
					teams[0].invalidation();
					teams[1].invalidation();
					teams[1].keySelectAgents(0);
					selectingPoint = teams[1].agentsGeter()[0].nowPoint;
					field.lightUp(selectingPoint);
				}
				else if (Key4.down()) {
					teams[0].invalidation();
					teams[1].invalidation();
					teams[1].keySelectAgents(1);
					selectingPoint = teams[1].agentsGeter()[1].nowPoint;
					field.lightUp(selectingPoint);
				}

				if (KeyNum1.down() && field.selectingField({ selectingPoint.x - 1, selectingPoint.y + 1 })) {
					field.selectReset();
					selectingPoint = { selectingPoint.x - 1, selectingPoint.y + 1 };
					if (!Procon29::VSComputer) {
						teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
					}
					teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
				}
				else if (KeyNum2.down() && field.selectingField({ selectingPoint.x, selectingPoint.y + 1 })) {
					field.selectReset();
					selectingPoint = { selectingPoint.x, selectingPoint.y + 1 };
					if (!Procon29::VSComputer) {
						teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
					}
					teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
				}
				else if (KeyNum3.down() && field.selectingField({ selectingPoint.x + 1, selectingPoint.y + 1 })) {
					field.selectReset();
					selectingPoint = { selectingPoint.x + 1, selectingPoint.y + 1 };
					if (!Procon29::VSComputer) {
						teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
					}
					teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
				}
				else if (KeyNum4.down() && field.selectingField({ selectingPoint.x - 1, selectingPoint.y })) {
					field.selectReset();
					selectingPoint = { selectingPoint.x - 1, selectingPoint.y };
					if (!Procon29::VSComputer) {
						teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
					}
					teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
				}
				else if (KeyNum5.down()) {
					field.selectReset();
					if (!Procon29::VSComputer) {
						teams[0].changeStay();
					}
					teams[1].changeStay();
				}
				else if (KeyNum6.down() && field.selectingField({ selectingPoint.x + 1, selectingPoint.y })) {
					field.selectReset();
					selectingPoint = { selectingPoint.x + 1, selectingPoint.y };
					if (!Procon29::VSComputer) {
						teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
					}
					teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
				}
				else if (KeyNum7.down() && field.selectingField({ selectingPoint.x - 1, selectingPoint.y - 1 })) {
					field.selectReset();
					selectingPoint = { selectingPoint.x - 1, selectingPoint.y - 1 };
					if (!Procon29::VSComputer) {
						teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
					}
					teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
				}
				else if (KeyNum8.down() && field.selectingField({ selectingPoint.x , selectingPoint.y - 1 })) {
					field.selectReset();
					selectingPoint = { selectingPoint.x, selectingPoint.y - 1 };
					if (!Procon29::VSComputer) {
						teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
					}
					teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
				}
				else if (KeyNum9.down() && field.selectingField({ selectingPoint.x + 1, selectingPoint.y - 1 })) {
					field.selectReset();
					selectingPoint = { selectingPoint.x + 1, selectingPoint.y - 1 };
					if (!Procon29::VSComputer) {
						teams[0].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
					}
					teams[1].moving(selectingPoint, field.teamTypeGetter(selectingPoint));
				}

			}
		}
		senjuKaneko.update(teams, field);

		//Draw
		field.draw();
		teams[0].draw();
		teams[1].draw();
		FontAsset(U"Fieldfont")(U"時間経過 : ", gameManager.timerGetter()).draw(12 * 64 + 10, 160, Palette::Black);
		//FontAsset(U"Fieldfont")(selectingPoint).draw(12 * 64 + 10, 210, Palette::Black);
		FontAsset(U"Fieldfont")(U"ターン数 : ", gameManager.turnGetter()).draw(12 * 64 + 10, 190, Palette::Black);

		FontAsset(U"Fieldfont")(isSuperUserMode ? U"SuperUserMode" : U"DefaultMode").draw(Vec2(Procon29::TileSize * Procon29::MaxFieldX + Procon29::SideAreaX / 6, Procon29::TileSize * Procon29::MaxFieldY / 2 + 50),Palette::Green);
		if (SimpleGUI::Button(U"Skip", Vec2(Procon29::TileSize * Procon29::MaxFieldX + Procon29::SideAreaX / 6, Procon29::TileSize * Procon29::MaxFieldY / 2 + 100), Procon29::buttonSizeX, !Procon29::VSComputer) || (!Procon29::VSComputer && KeyZ.down())) {
			saveData.save(teams, field, calculateResults);
			gameManager.update(teams, field);
		}

		if (SimpleGUI::Button(U"Prev", Vec2(Procon29::TileSize * Procon29::MaxFieldX + Procon29::SideAreaX / 6, Procon29::TileSize * Procon29::MaxFieldY / 2 + 200), Procon29::buttonSizeX, (isResultAcquired && isSubResultAcquired) || computer.getState() == Procon29::Computer::ComputerState::Null) || (computer.getState() == Procon29::Computer::ComputerState::Null && KeyB.down())) {
			gameManager.prev(teams, field, calculateResults, saveData);
		}
		if (SimpleGUI::Button(U"Calc", Vec2(Procon29::TileSize * Procon29::MaxFieldX + Procon29::SideAreaX / 6, Procon29::TileSize * Procon29::MaxFieldY / 2 + 300), Procon29::buttonSizeX,
			!isSuperUserMode && ((isResultAcquired && isSubResultAcquired) || computer.getState() == Procon29::Computer::ComputerState::Null))
			|| (!isSuperUserMode &&  ((computer.getState() == Procon29::Computer::ComputerState::Null && (KeyC.down() || KeySpace.down()))))) {
			saveData.save(teams, field, calculateResults);
			if (computer.getState() == Procon29::Computer::ComputerState::Null && !isResultAcquired) {

			}
			else
				gameManager.update(teams, field);
			computer.calculate(teams, field, gameManager.turnGetter());
			if (Procon29::ComputerVSComputer) {
				sub_computer.calculate(teams, field, gameManager.turnGetter());
				isSubResultAcquired = false;
			}
			isResultAcquired = false;
		}

		if (!isResultAcquired || !isSubResultAcquired) {
			if (Procon29::VSComputer) {
				switch (computer.getState()) {
				case Procon29::Computer::ComputerState::Ready:
					calculateResults = computer.getResult();
					for (auto calcer : calculateResults) {
						if (std::get<double>(calcer) == 1.0) {
							Point points[2] = { Point((int)std::get<Array<Vec3>>(calcer).at(0).x, (int)std::get<Array<Vec3>>(calcer).at(0).y) ,
								Point((int)std::get<Array<Vec3>>(calcer).at(1).x, (int)std::get<Array<Vec3>>(calcer).at(1).y) };
							teams[0].onMouseAgent(teams[0].agentsGeter().at(0).nowPoint);
							teams[0].moving(points[0], field.teamTypeGetter(points[0]));
							teams[0].onMouseAgent(teams[0].agentsGeter().at(1).nowPoint);
							teams[0].moving(points[1], field.teamTypeGetter(points[1]));
							teams[0].invalidation();
						}
					}
					isResultAcquired = true;
					break;
				case Procon29::Computer::ComputerState::Calc:
					FontAsset(U"Fieldfont")(U"Calculatenow").draw(Procon29::MaxFieldX * Procon29::TileSize + 10 + Procon29::SideAreaX, Procon29::TileSize * Procon29::MaxFieldY / 2 + 100, Palette::Black);
					break;
				case Procon29::Computer::ComputerState::Null:
					FontAsset(U"Fieldfont")(U"Null").draw(Procon29::MaxFieldX * Procon29::TileSize + 10 + Procon29::SideAreaX, Procon29::TileSize * Procon29::MaxFieldY / 2 + 100, Palette::Black);
					break;
				}
			}
			else if (Procon29::ComputerVSComputer) {
				switch (computer.getState()) {
				case Procon29::Computer::ComputerState::Ready:
					calculateResults = computer.getResult();
					for (auto calcer : calculateResults) {
						if (std::get<double>(calcer) == 1.0) {
							Point points[2] = { Point((int)std::get<Array<Vec3>>(calcer).at(0).x, (int)std::get<Array<Vec3>>(calcer).at(0).y) ,
								Point((int)std::get<Array<Vec3>>(calcer).at(1).x, (int)std::get<Array<Vec3>>(calcer).at(1).y) };
							teams[0].onMouseAgent(teams[0].agentsGeter().at(0).nowPoint);
							teams[0].moving(points[0], field.teamTypeGetter(points[0]));
							teams[0].onMouseAgent(teams[0].agentsGeter().at(1).nowPoint);
							teams[0].moving(points[1], field.teamTypeGetter(points[1]));
							teams[0].invalidation();
						}
					}
					isResultAcquired = true;
					break;
				case Procon29::Computer::ComputerState::Calc:
					FontAsset(U"Fieldfont")(U"Calculatenow").draw(Procon29::MaxFieldX * Procon29::TileSize + 10 + Procon29::SideAreaX, Procon29::TileSize * Procon29::MaxFieldY / 2 + 100, Palette::Black);
					break;
				case Procon29::Computer::ComputerState::Null:
					FontAsset(U"Fieldfont")(U"Null").draw(Procon29::MaxFieldX * Procon29::TileSize + 10 + Procon29::SideAreaX, Procon29::TileSize * Procon29::MaxFieldY / 2 + 100, Palette::Black);
					break;
				}

				switch (sub_computer.getState()) {
				case Procon29::Computer::ComputerState::Ready:
					calculateSubResults = sub_computer.getResult();
					for (auto calcer : calculateSubResults) {
						if (std::get<double>(calcer) == 1.0) {
							Point points[2] = { Point((int)std::get<Array<Vec3>>(calcer).at(0).x, (int)std::get<Array<Vec3>>(calcer).at(0).y) ,
								Point((int)std::get<Array<Vec3>>(calcer).at(1).x, (int)std::get<Array<Vec3>>(calcer).at(1).y) };
							teams[1].onMouseAgent(teams[1].agentsGeter().at(0).nowPoint);
							teams[1].moving(points[0], field.teamTypeGetter(points[0]));
							teams[1].onMouseAgent(teams[1].agentsGeter().at(1).nowPoint);
							teams[1].moving(points[1], field.teamTypeGetter(points[1]));
							teams[1].invalidation();
						}
					}
					isSubResultAcquired = true;
					break;
				case Procon29::Computer::ComputerState::Calc:
					FontAsset(U"Fieldfont")(U"SubCalculatenow").draw(Procon29::MaxFieldX * Procon29::TileSize + 10 + Procon29::SideAreaX, Procon29::TileSize * Procon29::MaxFieldY / 2 + 150, Palette::Black);
					break;
				case Procon29::Computer::ComputerState::Null:
					FontAsset(U"Fieldfont")(U"Null").draw(Procon29::MaxFieldX * Procon29::TileSize + 10 + Procon29::SideAreaX, Procon29::TileSize * Procon29::MaxFieldY / 2 + 150, Palette::Black);
					break;
				}
			}
			else {
				const int computerInfoX = Procon29::MaxFieldX * Procon29::TileSize + Procon29::SideAreaX / 6;
				String output;
				switch (computer.getState()) {
				case Procon29::Computer::ComputerState::Ready:
					output += U"Ready";
					calculateResults = computer.getResult();
					for (auto calcer : calculateResults) {
						output += U"\n" + ToString(std::get<double>(calcer));
						for (auto recommendPoint : std::get<Array<Vec3>>(calcer)) {
							output += U" (" + ToString(recommendPoint.x) + U"," + ToString(recommendPoint.y) + U")";

							//if (std::get<double>(calcer) == 1.0)
							{
								Point points[2] = { Point((int)std::get<Array<Vec3>>(calcer).at(0).x, (int)std::get<Array<Vec3>>(calcer).at(0).y) ,
									Point((int)std::get<Array<Vec3>>(calcer).at(1).x, (int)std::get<Array<Vec3>>(calcer).at(1).y) };
								if ((int)std::get<Array<Vec3>>(calcer).at(0).z != 0) {
									teams[0].onMouseAgent(teams[0].agentsGeter().at(0).nowPoint);
									teams[0].moving(points[0], field.teamTypeGetter(points[0]));
								}
								if ((int)std::get<Array<Vec3>>(calcer).at(1).z != 0) {
									teams[0].onMouseAgent(teams[0].agentsGeter().at(1).nowPoint);
									teams[0].moving(points[1], field.teamTypeGetter(points[1]));
								}
								teams[0].invalidation();
							}
							break;
						}
					}
					//Print << output;
					isResultAcquired = true;
					break;
				case Procon29::Computer::ComputerState::Calc:
					FontAsset(U"Fieldfont")(U"Calculatenow").draw(computerInfoX, Procon29::TileSize * Procon29::MaxFieldY / 2 - 100, Palette::Black);
					break;
				case Procon29::Computer::ComputerState::Null:
					FontAsset(U"Fieldfont")(U"Null").draw(computerInfoX, Procon29::TileSize * Procon29::MaxFieldY / 2 - 100, Palette::Black);
					break;
				}
			}
		}
		else {
			const int computerInfoX = Procon29::MaxFieldX * Procon29::TileSize + Procon29::SideAreaX / 6;

			FontAsset(U"Fieldfont")(U"ResultAcquried").draw(computerInfoX, Procon29::TileSize * Procon29::MaxFieldY / 2 - 100, Palette::Black);
			if (!Procon29::VSComputer) {
				FontAsset(U"Fieldfont")(U"size : ", calculateResults.size()).draw(computerInfoX, Procon29::TileSize * Procon29::MaxFieldY / 2 - 20, Palette::Black);
				for (auto calcer : calculateResults) {
					for (auto recommendVec3 : std::get<Array<Vec3>>(calcer)) {
						HSV coverHSV = HSV(coverColor);
						Point recommendPoint = Point((int)recommendVec3.x, (int)recommendVec3.y);
						coverHSV.s = std::get<double>(calcer);
						coverRect.movedBy(Procon29::TileSize * (recommendPoint)+Point(Procon29::TileSize / 2, Procon29::TileSize / 2)).draw(coverHSV);
					}
				}
			}
			if (Procon29::ComputerVSComputer) {
				FontAsset(U"Fieldfont")(U"SubResultAcquried").draw(computerInfoX, Procon29::TileSize * Procon29::MaxFieldY / 2 - 60, Palette::Black);
				FontAsset(U"Fieldfont")(U"sub_size : ", calculateSubResults.size()).draw(computerInfoX, Procon29::TileSize * Procon29::MaxFieldY / 2 + 20, Palette::Black);
				for (auto calcer : calculateSubResults) {
					for (auto recommendVec3 : std::get<Array<Vec3>>(calcer)) {
						HSV coverHSV = HSV(sub_coverColor);
						Point recommendPoint = Point((int)recommendVec3.x, (int)recommendVec3.y);
						coverHSV.s = std::get<double>(calcer);
						coverRect.movedBy(Procon29::TileSize * (recommendPoint)+Point(Procon29::TileSize / 2, Procon29::TileSize / 2)).draw(coverHSV);
					}
				}

				if (Procon29::AutoComputerVSComputer) {
					saveData.save(teams, field, calculateResults);
					gameManager.update(teams, field);
					computer.calculate(teams, field, gameManager.turnGetter());
					sub_computer.calculate(teams, field, gameManager.turnGetter());
					isSubResultAcquired = false;
					isResultAcquired = false;
				}
			}
		}

		senjuKaneko.draw();
	}
}
