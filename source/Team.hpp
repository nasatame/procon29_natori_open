#pragma once

#include "Procon29.hpp"
#include "Field.hpp"
#include "Agent.hpp"

namespace Procon29 {
	class Team {
	private:

		Array<Procon29::Agent> m_agents;
		int32 m_score;
		Procon29::TeamType m_teamType;
		Rect m_teamInfomation{ SideAreaX ,MaxFieldY *TileSize / 3 };
		bool loadAgents(String fileName);
		String drawSupporter(Procon29::Agent agent) const;

		bool isColorReverse;

	public:

		Team(Procon29::TeamType teamType, String fileName);
		Team(String initilizeString, bool isInvert);
		Team();
		void culcScore(const Field &field);
		void draw() const;
		void update(bool colorFlag);
		const Procon29::TeamType teamTypeGetter() const;
		void changePeel();
		void changeStay();
		void changeMove();
		void keySelectAgents(int32 num);
		Point nowSelectAgent() const;
		void invalidation();
		void moving(Point pos, TeamType tileTeamType);
		bool onMouseAgent(Point pos);
		const Array<Agent>& agentsGeter() const;
		void moveAgents(Array<int32> actions,int32 team);

		//危険な関数！！！
		void forcedAgentPointChange(Point pos, bool superUserFlag);
		//危険な関数！！！
		//直訳：強制的にフィールドに合理性を与える。
		void forcedGiveRationakityToTheField(Field &field,bool superUserFlag);

		[[nodiscard]] String format() const;
		const int32 getScore() const;

		//チームを90度回転する。
		void rotate90(const Field& field);
	};

	using Teams = Array<Procon29::Team>;

}
