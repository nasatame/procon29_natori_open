#pragma once

#include "Procon29.hpp"
#include "Team.hpp"
#include "Field.hpp"
#include "Computer.hpp"

//過去１ターンのみ残して復元できるように
namespace Procon29 {
	class SaveData {
	private:
		Teams teams;
		Field field;
		Computer::ResultType result;
	public:
		void save(Teams saveTeams, Field saveField, Computer::ResultType saveResult);
		void load(Teams& loadTeams, Field& loadField, Computer::ResultType& loadResult);
	};
}

