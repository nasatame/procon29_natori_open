﻿#include "Agent.hpp"

bool Procon29::Agent::isMove() const
{
	return false;
}

void Procon29::Agent::draw(TeamType team, int number) const
{
	if (!(team == TeamType::MyTeam && VSComputer)) {
		if (selectedNextPoint) {
			m_agentRect.movedBy(nextPoint * TileSize).drawFrame(2, Palette::Yellow);
			Line((nowPoint.x + 1) * TileSize - TileSize / 2, (nowPoint.y + 1)* TileSize - TileSize / 2,
				(nextPoint.x + 1)* TileSize - TileSize / 2, (nextPoint.y + 1)* TileSize - TileSize / 2).drawArrow(5, { 10,10 }, Palette::Cyan);
		}
	}
	if ((team == TeamType::EnemyTeam && VSComputer) || !VSComputer) {
		FontAsset(U"TeamInfomation")((number == 1 ? U"①" : U"②")).draw(nowPoint.x * TileSize + 20, nowPoint.y * TileSize + 12, nowSelect ? ColorF(Palette::Black, 0.7) : ColorF(Palette::Lightyellow, 0.6));
	}
	else if ((team == TeamType::MyTeam && VSComputer)) {
		FontAsset(U"TeamInfomation")(U"✖").draw(nowPoint.x * TileSize + 20, nowPoint.y * TileSize + 12, ColorF(Palette::Lightyellow, 0.6));
	}
	//m_agentRect.movedBy(10,10).drawFrame(10,0,Palette::White);

}

void Procon29::Agent::update(Point pos)
{
	nextPoint = pos;
	selectedNextPoint = true;
}

void Procon29::Agent::moveing()
{
	if (action == 2) {
		nextPoint = nowPoint;
	}
	else if (action == 1) {
		nowPoint = nextPoint;
	}
	else if (action == 0) {
		nextPoint = nowPoint;
	}
	nowSelect = false;
	selectedNextPoint = false;
	action = 0;
}

Procon29::Agent::Agent(String initilizeString)
{
	Array<String> spritter = initilizeString.split(',');
	nowPoint = Point(Parse<int32>(spritter.at(0)), Parse<int32>(spritter.at(1)));
	nextPoint = Point(Parse<int32>(spritter.at(2)), Parse<int32>(spritter.at(3)));
	selectedNextPoint = Parse<bool>(spritter.at(4));
	nowSelect = Parse<bool>(spritter.at(5));
	action = Parse<int32>(spritter.at(6));
}

Procon29::Agent::Agent(Point initilizePoint)
{
	nowPoint = initilizePoint;
	nextPoint = initilizePoint;
	selectedNextPoint = false;
	action = 0;
	nowSelect = false;
}

String Procon29::Agent::format() const
{
	return Format(nowPoint.x, U",", nowPoint.y, U",", nextPoint.x, U",", nextPoint.y, U",", selectedNextPoint, U",", nowSelect, U",", action);
}
