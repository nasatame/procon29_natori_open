#pragma once

#include "Procon29.hpp"
#include "Field.hpp"
#include "Team.hpp"
#include "Computer.hpp"
#include "SaveData.hpp"
#include <string>
#include <type_traits>

namespace Procon29 {
	
	//つい出来てしまった神クラス
	//全知全能絶対伸
	class GameManager {
	private:

		//タイマー、ターン
		Stopwatch m_timer;
		Stopwatch m_turnTimer;
		int32 m_turn;
		//ログ出力
		struct Log {
			TileGrid board;
			Teams teams;
			Log(const TileGrid& b, const Teams& t);
		};
		Array<Log> m_Log;
		bool isPreved = true;

	public:

		String fileName;

		FilePath generateRandom();
		bool setting(Teams& teams, Field& field);
		TeamType judgeWinner(const Teams& teams);
		void outLog(const Field& field, const Teams& teams);
		bool convertQRToText(String fileName);
		String addEnemyTeam(String loadedText);
		Stopwatch timerGetter() const;
		int32 turnGetter() const;

		//update
		void update(Teams& teams, Field &field);
		void prev(Teams& teams, Field& field, Computer::ResultType& result, SaveData& saveData);
		void timerIncrement();
		void timerDecrement();

		//draw
		void drawScoreGraph();

	};

}

namespace s3d {

	//AgentのFormatter系統
	inline void Formatter(FormatData& formatData, const Procon29::Agent& value)
	{
		Formatter(formatData, value.format());
	};

	template <class CharType>
	inline std::basic_ostream<CharType>& operator <<(std::basic_ostream<CharType>& output, const Procon29::Agent& value)
	{
		return output << value.format();
	}

	//TeamのFormatter系統
	inline void Formatter(FormatData& formatData, const Procon29::Team& value)
	{
		Formatter(formatData, value.format());
	};

	template <class CharType>
	inline std::basic_ostream<CharType>& operator <<(std::basic_ostream<CharType>& output, const Procon29::Team& value)
	{
		return output << value.format();
	}

	//TileのFormatter系統
	inline void Formatter(FormatData& formatData, const Procon29::Tile& value)
	{
		Formatter(formatData, value.format());
	};

	template <class CharType>
	inline std::basic_ostream<CharType>& operator <<(std::basic_ostream<CharType>& output, const Procon29::Tile& value)
	{
		return output << value.format();
	}

	//FieldのFormatter系統
	inline void Formatter(FormatData& formatData, const Procon29::Field& value)
	{
		Formatter(formatData, value.format());
	};

	template <class CharType>
	inline std::basic_ostream<CharType>& operator <<(std::basic_ostream<CharType>& output, const Procon29::Field& value)
	{
		return output << value.format();
	}
}
