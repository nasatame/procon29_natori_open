//PerfectSearch�̉��ʌ݊�
#include "Computer.hpp"

Procon29::Computer::ResultType Procon29::Computer::Search::searchStupid(const Teams & teams, const Field & field, const Evaluate & evaluate)
{
	const size_t strategy_number = 100;
	const size_t result_size = 3;
	const size_t my_team = evaluate.my_team == TeamType::MyTeam ? 0 : 1;
	const size_t enemy_team = evaluate.my_team == TeamType::EnemyTeam ? 0 : 1;

	/*

	void print(double strategy_table[strategy_number]){
	for(int i = 0; i < strategy_number; i++){
	cout << strategy_table[i] << " ";
	}
	cout << endl;
	}

	void print(int table[strategy_number][strategy_number]){
	for(int i = 0; i < strategy_number; i++){
	for(int k = 0; k < strategy_number; k++)
	cout << table[i][k] << " ";
	cout << endl;
	}
	cout << endl;
	}

	*/

	int table[strategy_number][strategy_number] = {};
	double strategy_table[2][2][strategy_number] = {};

	int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < field.boardSizeGetter().x && p.y < field.boardSizeGetter().y);
	};

	auto convXY = [](const Point& p) {
		return p.x + p.y * 12;
	};

	InfoStupid first_state;
	first_state.x[0] = teams[my_team].agentsGeter()[0].nowPoint.x;
	first_state.y[0] = teams[my_team].agentsGeter()[0].nowPoint.y;
	first_state.x[1] = teams[my_team].agentsGeter()[1].nowPoint.x;
	first_state.y[1] = teams[my_team].agentsGeter()[1].nowPoint.y;
	first_state.enemy_x[0] = teams[enemy_team].agentsGeter()[0].nowPoint.x;
	first_state.enemy_y[0] = teams[enemy_team].agentsGeter()[0].nowPoint.y;
	first_state.enemy_x[1] = teams[enemy_team].agentsGeter()[1].nowPoint.x;
	first_state.enemy_y[1] = teams[enemy_team].agentsGeter()[1].nowPoint.y;
	for (auto i : step(field.boardSizeGetter())) {
		if (field.boardGetter().at(i).teamType != TeamType::None && field.boardGetter().at(i).teamType != evaluate.my_team) {
			first_state.enemy_area.set(convXY(i));
		}
		if (field.boardGetter().at(i).teamType == evaluate.my_team) {
			first_state.my_area.set(convXY(i));
		}
	}

	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {
			for (int dxy3 = 0; dxy3 < 9; dxy3++) {
				for (int dxy4 = 0; dxy4 < 9; dxy4++) {

					auto now_state = first_state;

					Point next_point[2] = {
						Point(now_state.x[0] + dxy[dxy1], now_state.y[0] + dxy[dxy1 + 1]),
						Point(now_state.x[1] + dxy[dxy2], now_state.y[1] + dxy[dxy2 + 1]) };

					Point enemy_next_point[2] = {
						Point(now_state.enemy_x[0] + dxy[dxy3], now_state.enemy_y[0] + dxy[dxy3 + 1]),
						Point(now_state.enemy_x[1] + dxy[dxy4], now_state.enemy_y[1] + dxy[dxy4 + 1]) };


					for (int i = 0; i < 2; i++)
						if (!checkIn(next_point[i])) {
							next_point[i] = Point(-1, -1);
						}

					for (int i = 0; i < 2; i++)
						if (!checkIn(enemy_next_point[i])) {
							enemy_next_point[i] = Point(-1, -1);
						}

					//erase double
					{
						if (next_point[0] == next_point[1]) {
							Point copy = next_point[0];
							next_point[0] = Point(-1, -1);
							next_point[1] = Point(-1, -1);

							if (enemy_next_point[0] == copy) {
								enemy_next_point[0] = Point(-1, -1);
							}
							if (enemy_next_point[1] == copy) {
								enemy_next_point[1] = Point(-1, -1);
							}
						}
						if (enemy_next_point[0] == enemy_next_point[1]) {
							Point copy = enemy_next_point[0];
							enemy_next_point[0] = Point(-1, -1);
							enemy_next_point[1] = Point(-1, -1);

							if (next_point[0] == copy) {
								next_point[0] = Point(-1, -1);
							}
							if (next_point[1] == copy) {
								next_point[1] = Point(-1, -1);
							}
						}
						if (enemy_next_point[0] == next_point[0]) {
							enemy_next_point[0] = Point(-1, -1);
							next_point[0] = Point(-1, -1);
						}
						if (enemy_next_point[0] == next_point[1]) {
							enemy_next_point[0] = Point(-1, -1);
							next_point[1] = Point(-1, -1);
						}
						if (enemy_next_point[1] == next_point[1]) {
							enemy_next_point[1] = Point(-1, -1);
							next_point[1] = Point(-1, -1);
						}
						if (enemy_next_point[1] == next_point[0]) {
							enemy_next_point[1] = Point(-1, -1);
							next_point[0] = Point(-1, -1);
						}
					}
					//my move
					for (int agent = 0; agent < 2; agent++) {

						if (next_point[agent] == Point(-1, -1))
							continue;

						bool wasMoved = now_state.my_area[convXY(next_point[agent])];

						bool isEnemyArea = now_state.enemy_area[convXY(next_point[agent])];

						if (isEnemyArea) {
							now_state.enemy_area.reset(convXY(next_point[agent]));
						}
						else {
							if (!wasMoved) {
								now_state.my_area.set(convXY(next_point[agent]));
							}
							else {
							}
						}
					}
					//enemy move
					for (int agent = 0; agent < 2; agent++) {

						if (enemy_next_point[agent] == Point(-1, -1))
							continue;

						bool wasMoved = now_state.enemy_area[convXY(enemy_next_point[agent])];

						bool isEnemyArea = now_state.my_area[convXY(enemy_next_point[agent])];

						if (isEnemyArea) {
							now_state.my_area.reset(convXY(enemy_next_point[agent]));
						}
						else {
							if (!wasMoved) {
								now_state.enemy_area.set(convXY(enemy_next_point[agent]));
							}
							else {
							}
						}
					}

					if (calcScore(field, now_state.enemy_area) < calcScore(field, now_state.my_area)) {
						table[dxy1 * 10 + dxy2][dxy3 * 10 + dxy4] = 1;
					}
				}
			}
		}
	}

	//aggregate
	for (int i = 0; i < strategy_number; i++) {
		double sum = 0;
		for (int k = 0; k < strategy_number; k++) {
			sum += table[i][k];
		}
		strategy_table[0][0][i] = sum / strategy_number;
	}
	//print(strategy_table[0][0]);

	for (int i = 0; i < strategy_number; i++) {
		double sum = 0;
		for (int k = 0; k < strategy_number; k++) {
			sum += 1 - table[k][i];
		}
		strategy_table[0][1][i] = sum / strategy_number;
	}
	//print(strategy_table[0][1]);

	{
		double sum = 0;
		for (int i = 0; i < strategy_number; i++) {
			sum += strategy_table[0][0][i];
		}
		for (int i = 0; i < strategy_number; i++) {
			strategy_table[0][0][i] /= sum;
		}
	}
	//print(strategy_table[0][0]);

	{
		double sum = 0;
		for (int i = 0; i < strategy_number; i++) {
			sum += strategy_table[0][1][i];
		}
		double st_max = 0;
		for (int i = 0; i < strategy_number; i++) {
			st_max = std::max(st_max, strategy_table[0][1][i] /= sum);
		}

		for (int i = 0; i < strategy_number; i++) {
			strategy_table[0][1][i] = strategy_table[0][1][i] == st_max ? strategy_table[0][1][i] * 2 : strategy_table[0][1][i];
		}

	}
	//print(strategy_table[0][1]);
	//cout << endl;

	//second
	for (int loop = 0; loop < 1; loop++) {

		const int now = loop % 2;
		const int next = (loop + 1) % 2;

		//cout << "num:" << i << endl;

		for (int i = 0; i < strategy_number; i++) {
			double sum = 0;
			for (int k = 0; k < strategy_number; k++) {
				sum += (table[i][k] == 1) ? strategy_table[now][1][k] : 0;
			}
			strategy_table[next][0][i] = sum;
		}
		//print(strategy_table[next][0]);

		for (int i = 0; i < strategy_number; i++) {
			double sum = 0;
			for (int k = 0; k < strategy_number; k++) {
				sum += (1 - table[k][i] == 1) ? strategy_table[now][0][k] : 0;
			}
			strategy_table[next][1][i] = sum;
		}
		//print(strategy_table[next][1]);

		{
			double sum = 0;
			for (int i = 0; i < strategy_number; i++) {
				sum += strategy_table[next][0][i];
			}
			if (sum == 0)sum = 1;
			for (int i = 0; i < strategy_number; i++) {
				strategy_table[next][0][i] /= sum;
			}
		}
		//print(strategy_table[next][0]);

		{
			double sum = 0;
			for (int i = 0; i < strategy_number; i++) {
				sum += strategy_table[next][1][i];
			}
			if (sum == 0)sum = 1;
			for (int i = 0; i < strategy_number; i++) {
				strategy_table[next][1][i] /= sum;
			}
		}
		//print(strategy_table[next][1]);
		//cout << endl;

	}

	ResultType result;
	double stmax = 0;
	for (int i = 0; i < 100; i++) {
		stmax = std::max(stmax, strategy_table[1][0][i]);
	}


	for (int i = 0; i < 100; i++) {

		if (stmax != strategy_table[1][0][i])
			continue;

		int action[2] = {};

		for (int agent = 0; agent < MaxAgentNum; agent++) {
			Point p;

			if (agent == 0) {
				p = (teams[my_team].agentsGeter()[0].nowPoint + Point(dxy[i / 10], dxy[i / 10 + 1]));
			}
			else {
				p = teams[my_team].agentsGeter()[1].nowPoint + Point(dxy[i % 10], dxy[(i % 10) + 1]);
			}

			//bool wasMoved = field.boardGetter()[p.y][p.x].teamType == TeamType::MyTeam;
			bool isEnemyArea = field.boardGetter()[p.y][p.x].teamType == TeamType::EnemyTeam;


			if (isEnemyArea)
				action[agent] = 2;
			else
				action[agent] = 1;
		}

		Array<Vec3> first_move_point = { Vec3(teams[my_team].agentsGeter()[0].nowPoint + Point(dxy[i / 10],dxy[i / 10 + 1]), action[0]),
			Vec3(teams[my_team].agentsGeter()[1].nowPoint + Point(dxy[i % 10],dxy[(i % 10) + 1]), action[1]) };
		bool same = false;
		for (int k = 0; k < result.size(); k++)
		{
			bool check = true;
			for (int m = 0; m < std::get<Array<Vec3>>(result[k]).size(); m++) {
				if (std::get<Array<Vec3>>(result[k])[m] != first_move_point[m]) {
					check = false;
				}
			}
			if (check) {
				same = true;
			}
		}
		if (same) {
			continue;
		}
		std::tuple<Array<Vec3>, double> candidate = {
			first_move_point,
			1 };
		result << candidate;
	}

	if (result.size() > result_size) {
		result.shuffle();
		result.erase(result.begin() + result_size, result.end());
	}

	return result;
}
