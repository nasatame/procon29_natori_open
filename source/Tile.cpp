#include "Tile.hpp"

String Procon29::Tile::format() const
{
	return Format(score, U"/", static_cast<int32>(teamType), U"/", exist, U"/", selecting);
}

Procon29::Tile::Tile(String initilizeString, bool isInvert)
{
	Array<String> spritter = initilizeString.split('/');
	score = Parse<int32>(spritter.at(0));
	teamType = static_cast<Procon29::TeamType>(Parse<int32>(spritter.at(1)));
	if (isInvert) {
		if (teamType == TeamType::MyTeam) {
			teamType = TeamType::EnemyTeam;
		}
		else if (teamType == TeamType::EnemyTeam) {
			teamType = TeamType::MyTeam;
		}
	}
	exist = Parse<bool>(spritter.at(2));
	selecting = Parse<bool>(spritter.at(3));
}

Procon29::Tile::Tile()
	:score(0)
	, teamType(TeamType::Null)
	, exist(false)
	, selecting(false)
{
};

Procon29::TeamType Procon29::Tile::teamTypeGetter() const
{
	return teamType;
}