#include "Procon29.hpp"

void Procon29::LoadTexture()
{
}

void Procon29::LoadFont()
{
	FontAsset::Register(U"QRInfo", 30, Typeface::Light);
	FontAsset::Register(U"Fieldfont", 30, Typeface::Light);
	FontAsset::Register(U"TeamInfomation", 40, Typeface::Black);
	FontAsset::Register(U"TurnInfomation", 80, Typeface::Black);
}

void Procon29::LoadData()
{
	LoadTexture();
	LoadFont();
}
