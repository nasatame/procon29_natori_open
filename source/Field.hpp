#pragma once

#include "Procon29.hpp"

namespace Procon29 {

	using TileGrid = Grid<Tile>;

	class Field {
	private:
		Size m_boardSize;
		TileGrid m_board{ MaxFieldX,MaxFieldY };
		Rect m_tileRect{ TileSize };
		bool load(String fileName);
		bool isColorReverse;
		bool isSuperUser;
	public:
		const TileGrid& boardGetter() const;
		TeamType teamTypeGetter(Point pos) const;
		void draw() const;
		//フィールドの更新
		void fieldUpdate(Array<Point> pos, Array<int32> action);
		//毎描画呼ばれる更新
		void update(bool colorFlag,bool superUserFlag);
		void lightUp(Point pos);
		bool selectingField(Point pos) const;
		bool canPeel(Point pos,Procon29::TeamType teamType);
		void selectReset();
		Point posConverter(Point pos);
		Size boardSizeGetter() const;
		void set(Point chengePoint, Procon29::TeamType changeType);
		Array<Point> getSurroundArea(const TeamType& teamType);

		[[nodiscard]] String format() const;

		Field() = default;
		//Field(const Procon29::Field field, bool isInvert);

		Field(String initilizeString, bool isInvert);
		Field(String fileName);

		//フィールドを90度回転する。
		void rotate90();
	};

}