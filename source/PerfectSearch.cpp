//最終ターン付近で全手読みのハズが、、、
//重すぎて最終１ターン全手読み。しかも、人間は５ターン読めるので弱い

#include <queue>
#include "Computer.hpp"

Procon29::Computer::ResultType Procon29::Computer::Search::searchPerfect(const Teams & teams, const Field & field, const Evaluate & evaluate)
{
	// 1 or 0
	const int search_depth = 1;
	const size_t my_team = evaluate.my_team == TeamType::MyTeam ? 0 : 1;
	const size_t enemy_team = evaluate.my_team == TeamType::EnemyTeam ? 0 : 1;

	if (search_depth > 3) {
		return ResultType();
	}

	int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < field.boardSizeGetter().x && p.y < field.boardSizeGetter().y);
	};

	auto convXY = [](const Point& p) {
		assert(0 <= p.x + p.y * 12 && p.x + p.y * 12 < 144);
		return p.x + p.y * 12;
	};

	InfoAlphaBeta first_state;
	first_state.first_dir[0] = -1;
	first_state.first_dir[1] = -1;
	first_state.my_x[0] = teams[my_team].agentsGeter()[0].nowPoint.x;
	first_state.my_y[0] = teams[my_team].agentsGeter()[0].nowPoint.y;
	first_state.my_x[1] = teams[my_team].agentsGeter()[1].nowPoint.x;
	first_state.my_y[1] = teams[my_team].agentsGeter()[1].nowPoint.y;
	first_state.enemy_x[0] = teams[enemy_team].agentsGeter()[0].nowPoint.x;
	first_state.enemy_y[0] = teams[enemy_team].agentsGeter()[0].nowPoint.y;
	first_state.enemy_x[1] = teams[enemy_team].agentsGeter()[1].nowPoint.x;
	first_state.enemy_y[1] = teams[enemy_team].agentsGeter()[1].nowPoint.y;
	for (auto i : step(field.boardSizeGetter())) {
		if (field.boardGetter().at(i).teamType != TeamType::None && field.boardGetter().at(i).teamType != evaluate.my_team) {
			first_state.enemy_area.set(convXY(i));
		}
		if (field.boardGetter().at(i).teamType == evaluate.my_team) {
			first_state.my_area.set(convXY(i));
		}
	}

	first_state.my_area_point = calcTileScore(field, first_state.my_area);
	first_state.enemy_area_point = calcTileScore(field, first_state.enemy_area);

	first_state.depth = search_depth;

	GlobalAlphaBeta global;

	global.game_turn = evaluate.game_turn;
	global.search_depth = MaxTurn - evaluate.game_turn - search_depth;
	global.board_size_y = field.boardSizeGetter().y;
	global.board_size_x = field.boardSizeGetter().x;

	global.alpha = -100000;
	global.beta = 100000;

	for (int i = 0; i < MaxFieldY; i++)
		std::fill(global.field[i], global.field[i] + MaxFieldX, -32);

	for (int i = 0; i < field.boardSizeGetter().y; i++)
		for (int j = 0; j < field.boardSizeGetter().x; j++) {
			global.field[i][j] = field.boardGetter()[i][j].score;
		}

	Array<InfoAlphaBeta> children;

	struct move {
		Point my_pos[2];
		Point before_pos[2];
		// 0 : stay , 1 : move , 2 : peel
		BYTE action[2];
		BYTE dir[2];
	};

	Array<move> myMoveCandidates = {};
	Array<move> enemyMoveCandidates = {};

	//自分チームの動き
	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {

			move candidate;

			Point next_point[2] = {
				Point(first_state.my_x[0] + dxy[dxy1], first_state.my_y[0] + dxy[dxy1 + 1]),
				Point(first_state.my_x[1] + dxy[dxy2], first_state.my_y[1] + dxy[dxy2 + 1]) };

			if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
				continue;

			//同じところに移動禁止
			if (next_point[0] == next_point[1])
				continue;

			for (int i = 0; i < MaxAgentNum; i++)
				candidate.before_pos[i] = candidate.my_pos[i] = Point(first_state.my_x[i], first_state.my_y[i]);

			candidate.dir[0] = dxy1;
			candidate.dir[1] = dxy2;

			//5パターン行動可能性がある
			//8^2で25パターンさすがに列挙はダルい
			{
				for (int pattern = 0; pattern < 64; pattern++) {
					bool skipFlag = false;
					int p = pattern;

					for (int i = 0; i < MaxAgentNum; i++) {
						if (p % 8 == 0) {
							if (first_state.my_area[convXY(next_point[i])] == 1) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 1) {
							if (first_state.my_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 2) {
							if (first_state.enemy_area[convXY(next_point[i])] == 1) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 3) {
							if (first_state.my_area[convXY(next_point[i])] == 0 && first_state.enemy_area[convXY(next_point[i])] == 0) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						/*
						else if (p % 8 == 4) {
							candidate.action[i] = 0;
						}
						*/
						else {
							skipFlag = true;
							break;
						}
						p >>= 3;
					}

					if (skipFlag)
						continue;

					assert(candidate.action[0] >= 0 && candidate.action[0] < 3);
					assert(candidate.action[1] >= 0 && candidate.action[1] < 3);
					myMoveCandidates.push_back(candidate);
				}
			}

		}
	}

	//敵チームの動き
	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {

			move candidate;

			Point next_point[2] = {
				Point(first_state.enemy_x[0] + dxy[dxy1], first_state.enemy_y[0] + dxy[dxy1 + 1]),
				Point(first_state.enemy_x[1] + dxy[dxy2], first_state.enemy_y[1] + dxy[dxy2 + 1]) };

			if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
				continue;

			//同じところに移動禁止
			if (next_point[0] == next_point[1])
				continue;

			for (int i = 0; i < MaxAgentNum; i++)
				candidate.before_pos[i] = candidate.my_pos[i] = Point(first_state.enemy_x[i], first_state.enemy_y[i]);
			candidate.dir[0] = dxy1;
			candidate.dir[1] = dxy2;

			//5パターン行動可能性がある
			//8^2で25パターンさすがに列挙はダルい
			{
				for (int pattern = 0; pattern < 64; pattern++) {
					bool skipFlag = false;
					int p = pattern;

					for (int i = 0; i < MaxAgentNum; i++) {
						if (p % 8 == 0) {
							if (first_state.enemy_area[convXY(next_point[i])] == 1) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 1) {
							if (first_state.enemy_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 2) {
							if (first_state.my_area[convXY(next_point[i])] == 1) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 3) {
							if (first_state.my_area[convXY(next_point[i])] == 0 && first_state.enemy_area[convXY(next_point[i])] == 0) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						/*
						else if (p % 8 == 4) {
							candidate.action[i] = 0;
						}
						*/
						else {
							skipFlag = true;
							break;
						}
						p >>= 3;
					}

					if (skipFlag)
						continue;

					enemyMoveCandidates.push_back(candidate);
				}
			}

		}
	}

	for (int i = 0; i < myMoveCandidates.size(); i++) {
		for (int j = 0; j < enemyMoveCandidates.size(); j++) {

			auto my_move = myMoveCandidates[i];
			auto enemy_move = enemyMoveCandidates[j];

			assert(my_move.action[0] >= 0 && my_move.action[0] < 3);
			assert(my_move.action[1] >= 0 && my_move.action[1] < 3);

			bool stop[2][2] = {};

			for (int a_i = 0; a_i < MaxAgentNum; a_i++) {
				for (int a_j = 0; a_j < MaxAgentNum; a_j++) {
					if (my_move.my_pos[a_i] == enemy_move.my_pos[a_j]) {
						stop[0][a_i] = true;
						stop[1][a_j] = true;
					}
				}
			}
			for (int a_i = 0; a_i < MaxAgentNum; a_i++) {
				for (int a_j = 0; a_j < MaxAgentNum; a_j++) {
					if (stop[0][a_i] && my_move.before_pos[a_i] == enemy_move.my_pos[a_j]) {
						stop[1][a_j] = true;
					}
					if (stop[1][a_j] && my_move.my_pos[a_i] == enemy_move.before_pos[a_j]) {
						stop[0][a_i] = true;
					}
				}
			}

			//移動先が確定したのでシミュレーションする。
			InfoAlphaBeta now_state = first_state;

			now_state.depth -= 1;
			now_state.first_dir[0] = my_move.dir[0];
			now_state.first_dir[1] = my_move.dir[1];

			for (int a_i = 0; a_i < MaxAgentNum; a_i++) {
				now_state.action[a_i] = my_move.action[a_i];
				if (stop[0][a_i])
					continue;
				switch (my_move.action[a_i]) {
				case 0:
					break;
				case 1:
					now_state.my_x[a_i] = my_move.my_pos[a_i].x;
					now_state.my_y[a_i] = my_move.my_pos[a_i].y;
					if (now_state.my_area[convXY(my_move.my_pos[a_i])] == 0) {
						now_state.my_area[convXY(my_move.my_pos[a_i])] = 1;
						now_state.my_area_point += global.field[my_move.my_pos[a_i].y][my_move.my_pos[a_i].x];
					}
					break;
				case 2:
					if (now_state.my_area[convXY(my_move.my_pos[a_i])] == 1) {
						now_state.my_area[convXY(my_move.my_pos[a_i])] = 0;
						now_state.my_area_point -= global.field[my_move.my_pos[a_i].y][my_move.my_pos[a_i].x];
					}
					else if (now_state.enemy_area[convXY(my_move.my_pos[a_i])] == 1) {
						now_state.enemy_area[convXY(my_move.my_pos[a_i])] = 0;
						now_state.enemy_area_point -= global.field[my_move.my_pos[a_i].y][my_move.my_pos[a_i].x];
					}
					break;
				default:
					assert(false);
					break;
				}
			}

			for (int a_i = 0; a_i < MaxAgentNum; a_i++) {
				if (stop[1][a_i])
					continue;
				switch (enemy_move.action[a_i]) {
				case 0:
					break;
				case 1:
					now_state.enemy_x[a_i] = enemy_move.my_pos[a_i].x;
					now_state.enemy_y[a_i] = enemy_move.my_pos[a_i].y;
					if (now_state.enemy_area[convXY(enemy_move.my_pos[a_i])] == 0) {
						now_state.enemy_area[convXY(enemy_move.my_pos[a_i])] = 1;
						now_state.enemy_area_point += global.field[enemy_move.my_pos[a_i].y][enemy_move.my_pos[a_i].x];
					}
					break;
				case 2:
					if (now_state.enemy_area[convXY(enemy_move.my_pos[a_i])] == 1) {
						now_state.enemy_area[convXY(enemy_move.my_pos[a_i])] = 0;
						now_state.enemy_area_point -= global.field[enemy_move.my_pos[a_i].y][enemy_move.my_pos[a_i].x];
					}
					else if (now_state.my_area[convXY(enemy_move.my_pos[a_i])] == 1) {
						now_state.my_area[convXY(enemy_move.my_pos[a_i])] = 0;
						now_state.my_area_point -= global.field[enemy_move.my_pos[a_i].y][enemy_move.my_pos[a_i].x];
					}
					break;
				}
			}

			assert(now_state.action[0] >= 0 && now_state.action[0] < 3);
			assert(now_state.action[1] >= 0 && now_state.action[1] < 3);
			children << now_state;

		}
	}

	//並び替え
	sort(children.begin(), children.end(), [](const InfoAlphaBeta& left, const InfoAlphaBeta& right) {
		return (left.my_area_point - left.enemy_area_point) > (right.my_area_point - right.enemy_area_point);
	});

	//高速化の工夫
	//並列化

	int maxNum[4] = {};
	double total[9][9][3][3] = {};
	int total_sum[9][9][3][3] = {};

	const size_t core_num = 10;
	size_t task_size = children.size();
	Array<std::future<ResultAlphaBeta>> arr_result(core_num);

	for (int i = 0; i < children.size(); i++) {
		total_sum[children[i].first_dir[0]][children[i].first_dir[1]][children[i].action[0]][children[i].action[1]]++;
	}

	for (int loop = 0; loop < task_size / core_num; loop++) {
		for (int i = loop * core_num; i < std::min(task_size, (loop + 1) * core_num); i++) {
			arr_result[i - loop * core_num] = (std::async(std::launch::async,
				[](InfoAlphaBeta info, GlobalAlphaBeta global) {return Search().funcSearchPerfect(info, global); },
				children[i], global));
		}
		for (int i = 0; i < core_num; i++) {
			ResultAlphaBeta result = arr_result[i].get();
			assert(!std::isnan(result.score / total_sum[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]]));
			total[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]] += result.score / total_sum[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]];
			if (total[maxNum[0]][maxNum[1]][maxNum[2]][maxNum[3]] < total[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]]) {
				maxNum[0] = result.first_dir[0];
				maxNum[1] = result.first_dir[1];
				maxNum[2] = result.action[0];
				maxNum[3] = result.action[1];
			}

			if (abs(total[maxNum[0]][maxNum[1]][maxNum[2]][maxNum[3]] - 1.0) < 0.0000001) {
				break;
			}
		}
	}
	/*
	for (int i = 0; i < ; i++) {
		ResultAlphaBeta result = funcSearchPerfect(children[i], global);
		{
			assert(result.first_dir[0] >= 0 && result.first_dir[0] < 9);
			a-ssert(result.first_dir[1] >= 0 && result.first_dir[1] < 9);
			assert(result.action[0] >= 0 && result.action[0] < 3);
			assert(result.action[1] >= 0 && result.action[1] < 3);
			total[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]]++;

			if (total[maxNum[0]][maxNum[1]][maxNum[2]][maxNum[3]] < total[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]]) {
				maxNum[0] = result.first_dir[0];
				maxNum[1] = result.first_dir[1];
				maxNum[2] = result.action[0];
				maxNum[3] = result.action[1];
			}
		}
	}
	*/
	ResultType result;
	{
		Array<Vec3> first_move_point = { Vec3(teams[0].agentsGeter()[0].nowPoint + Point(dxy[maxNum[0]],dxy[maxNum[0] + 1]), maxNum[2]),
			Vec3(teams[0].agentsGeter()[1].nowPoint + Point(dxy[maxNum[1]],dxy[maxNum[1] + 1]),maxNum[3]) };

		std::tuple<Array<Vec3>, double> candidate = {
			first_move_point,
			total[maxNum[0]][maxNum[1]][maxNum[2]][maxNum[3]] };
		result << candidate;

	}
	return result;
}

Procon29::Computer::Search::ResultAlphaBeta Procon29::Computer::Search::funcSearchPerfect(const InfoAlphaBeta & info, GlobalAlphaBeta & global)
{
	const int search_depth = info.depth;
	const int beam_demerit = 0.01;

	int dxy[10] = { 1,-1,-1,0,-1,1,0,0,1,1 };

	auto checkIn = [=](const Point& p) {
		return (0 <= p.x && 0 <= p.y && p.x < global.board_size_x && p.y < global.board_size_y);
	};

	auto convXY = [](const Point& p) {
		assert(0 <= p.x + p.y * 12 && p.x + p.y * 12 < 144);
		return p.x + p.y * 12;
	};

	//終了条件
	if (search_depth <= 0) {
		ResultAlphaBeta result;
		double my_eval = calcScore(global.field, info.my_area);
		double enemy_eval = calcScore(global.field, info.enemy_area);

		result.first_dir[0] = info.first_dir[0];
		result.first_dir[1] = info.first_dir[1];
		result.action[0] = info.action[0];
		result.action[1] = info.action[1];

		if (MaxTurn - global.game_turn != 1) {
			auto f_my = std::async(std::launch::async,
				[](InfoAlphaBeta info, GlobalAlphaBeta global) {
				InfoBeamSearch beam_info;
				beam_info.enemy_area = info.enemy_area;
				beam_info.my_area = info.my_area;

				for (int i = 0; i < MaxAgentNum; i++) {
					beam_info.x[i] = info.my_x[i];
					beam_info.y[i] = info.my_y[i];
				}

				Point enemyPos[2] = { Point(info.enemy_x[0],info.enemy_y[0]),Point(info.enemy_x[1],info.enemy_y[1]) };

				return Search().evaluateBeamSearch(beam_info, global, enemyPos);
			},
				info, global);

			auto f_enemy = std::async(std::launch::async,
				[](InfoAlphaBeta info, GlobalAlphaBeta global) {
				InfoBeamSearch beam_info;
				beam_info.enemy_area = info.my_area;
				beam_info.my_area = info.enemy_area;

				for (int i = 0; i < MaxAgentNum; i++) {
					beam_info.x[i] = info.enemy_x[i];
					beam_info.y[i] = info.enemy_y[i];
				}

				Point enemyPos[2] = { Point(info.my_x[0],info.my_y[0]),Point(info.my_x[1],info.my_y[1]) };

				return Search().evaluateBeamSearch(beam_info, global, enemyPos);
			},
				info, global);

			{
				my_eval += f_my.get() * beam_demerit;
				enemy_eval += f_enemy.get() * beam_demerit;
			}
		}

		if (my_eval - enemy_eval > 0)
			result.score = 1.0;
		else
			result.score = Max(0.0, my_eval / enemy_eval);
		return result;
	}


	ResultAlphaBeta optimal;

	Array<InfoAlphaBeta> children;

	struct move {
		Point my_pos[2];
		Point before_pos[2];
		// 0 : stay , 1 : move , 2 : peel
		BYTE action[2];
		BYTE dir[2];
	};

	Array<move> myMoveCandidates = {};
	Array<move> enemyMoveCandidates = {};

	const auto & first_state = info;

	//自分チームの動き
	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {

			move candidate;

			Point next_point[2] = {
				Point(first_state.my_x[0] + dxy[dxy1], first_state.my_y[0] + dxy[dxy1 + 1]),
				Point(first_state.my_x[1] + dxy[dxy2], first_state.my_y[1] + dxy[dxy2 + 1]) };

			if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
				continue;

			//同じところに移動禁止
			if (next_point[0] == next_point[1])
				continue;

			for (int i = 0; i < MaxAgentNum; i++)
				candidate.before_pos[i] = candidate.my_pos[i] = Point(first_state.my_x[i], first_state.my_y[i]);

			candidate.dir[0] = dxy1;
			candidate.dir[1] = dxy2;

			//5パターン行動可能性がある
			//8^2で25パターンさすがに列挙はダルい
			{
				for (int pattern = 0; pattern < 64; pattern++) {
					bool skipFlag = false;
					int p = pattern;

					for (int i = 0; i < MaxAgentNum; i++) {
						if (p % 8 == 0) {
							if (first_state.my_area[convXY(next_point[i])] == 1) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 1) {
							if (first_state.my_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 2) {
							if (first_state.enemy_area[convXY(next_point[i])] == 1) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 3) {
							if (first_state.my_area[convXY(next_point[i])] == 0 && first_state.enemy_area[convXY(next_point[i])] == 0) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						/*
						else if (p % 8 == 4) {
							candidate.action[i] = 0;
						}
						*/
						else {
							skipFlag = true;
							break;
						}
						p >>= 3;
					}

					if (skipFlag)
						continue;

					assert(candidate.action[0] >= 0 && candidate.action[0] < 3);
					assert(candidate.action[1] >= 0 && candidate.action[1] < 3);
					myMoveCandidates.push_back(candidate);
				}
			}

		}
	}

	//敵チームの動き
	for (int dxy1 = 0; dxy1 < 9; dxy1++) {
		for (int dxy2 = 0; dxy2 < 9; dxy2++) {

			move candidate;

			Point next_point[2] = {
				Point(first_state.enemy_x[0] + dxy[dxy1], first_state.enemy_y[0] + dxy[dxy1 + 1]),
				Point(first_state.enemy_x[1] + dxy[dxy2], first_state.enemy_y[1] + dxy[dxy2 + 1]) };

			if (!checkIn(next_point[0]) || !checkIn(next_point[1]))
				continue;

			//同じところに移動禁止
			if (next_point[0] == next_point[1])
				continue;

			for (int i = 0; i < MaxAgentNum; i++)
				candidate.before_pos[i] = candidate.my_pos[i] = Point(first_state.enemy_x[i], first_state.enemy_y[i]);
			candidate.dir[0] = dxy1;
			candidate.dir[1] = dxy2;

			//5パターン行動可能性がある
			//8^2で25パターンさすがに列挙はダルい
			{
				for (int pattern = 0; pattern < 64; pattern++) {
					bool skipFlag = false;
					int p = pattern;

					for (int i = 0; i < MaxAgentNum; i++) {
						if (p % 8 == 0) {
							if (first_state.enemy_area[convXY(next_point[i])] == 1) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 1) {
							if (first_state.enemy_area[convXY(next_point[i])] == 1 && global.field[next_point[i].y][next_point[i].x] < 0) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 2) {
							if (first_state.my_area[convXY(next_point[i])] == 1) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 2;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						else if (p % 8 == 3) {
							if (first_state.my_area[convXY(next_point[i])] == 0 && first_state.enemy_area[convXY(next_point[i])] == 0) {
								candidate.my_pos[i] = next_point[i];
								candidate.action[i] = 1;
							}
							else {
								skipFlag = true;
								break;
							}
						}
						/*
						else if (p % 8 == 4) {
							candidate.action[i] = 0;
						}
						*/
						else {
							skipFlag = true;
							break;
						}
						p >>= 3;
					}

					if (skipFlag)
						continue;

					enemyMoveCandidates.push_back(candidate);
				}
			}

		}
	}

	for (int i = 0; i < myMoveCandidates.size(); i++) {
		for (int j = 0; j < enemyMoveCandidates.size(); j++) {

			auto my_move = myMoveCandidates[i];
			auto enemy_move = enemyMoveCandidates[j];

			assert(my_move.action[0] >= 0 && my_move.action[0] < 3);
			assert(my_move.action[1] >= 0 && my_move.action[1] < 3);

			bool stop[2][2] = {};

			for (int a_i = 0; a_i < MaxAgentNum; a_i++) {
				for (int a_j = 0; a_j < MaxAgentNum; a_j++) {
					if (my_move.my_pos[a_i] == enemy_move.my_pos[a_j]) {
						stop[0][a_i] = true;
						stop[1][a_j] = true;
					}
				}
			}
			for (int a_i = 0; a_i < MaxAgentNum; a_i++) {
				for (int a_j = 0; a_j < MaxAgentNum; a_j++) {
					if (stop[0][a_i] && my_move.before_pos[a_i] == enemy_move.my_pos[a_j]) {
						stop[1][a_j] = true;
					}
					if (stop[1][a_j] && my_move.my_pos[a_i] == enemy_move.before_pos[a_j]) {
						stop[0][a_i] = true;
					}
				}
			}

			//移動先が確定したのでシミュレーションする。
			InfoAlphaBeta now_state = first_state;

			now_state.depth -= 1;

			for (int a_i = 0; a_i < MaxAgentNum; a_i++) {
				if (stop[0][a_i])
					continue;
				switch (my_move.action[a_i]) {
				case 0:
					break;
				case 1:
					now_state.my_x[a_i] = my_move.my_pos[a_i].x;
					now_state.my_y[a_i] = my_move.my_pos[a_i].y;
					if (now_state.my_area[convXY(my_move.my_pos[a_i])] == 0) {
						now_state.my_area[convXY(my_move.my_pos[a_i])] = 1;
						now_state.my_area_point += global.field[my_move.my_pos[a_i].y][my_move.my_pos[a_i].x];
					}
					break;
				case 2:
					if (now_state.my_area[convXY(my_move.my_pos[a_i])] == 1) {
						now_state.my_area[convXY(my_move.my_pos[a_i])] = 0;
						now_state.my_area_point -= global.field[my_move.my_pos[a_i].y][my_move.my_pos[a_i].x];
					}
					else if (now_state.enemy_area[convXY(my_move.my_pos[a_i])] == 1) {
						now_state.enemy_area[convXY(my_move.my_pos[a_i])] = 0;
						now_state.enemy_area_point -= global.field[my_move.my_pos[a_i].y][my_move.my_pos[a_i].x];
					}
					break;
				default:
					assert(false);
					break;
				}
			}

			for (int a_i = 0; a_i < MaxAgentNum; a_i++) {
				if (stop[1][a_i])
					continue;
				switch (enemy_move.action[a_i]) {
				case 0:
					break;
				case 1:
					now_state.enemy_x[a_i] = enemy_move.my_pos[a_i].x;
					now_state.enemy_y[a_i] = enemy_move.my_pos[a_i].y;
					if (now_state.enemy_area[convXY(enemy_move.my_pos[a_i])] == 0) {
						now_state.enemy_area[convXY(enemy_move.my_pos[a_i])] = 1;
						now_state.enemy_area_point += global.field[enemy_move.my_pos[a_i].y][enemy_move.my_pos[a_i].x];
					}
					break;
				case 2:
					if (now_state.enemy_area[convXY(enemy_move.my_pos[a_i])] == 1) {
						now_state.enemy_area[convXY(enemy_move.my_pos[a_i])] = 0;
						now_state.enemy_area_point -= global.field[enemy_move.my_pos[a_i].y][enemy_move.my_pos[a_i].x];
					}
					else if (now_state.my_area[convXY(enemy_move.my_pos[a_i])] == 1) {
						now_state.my_area[convXY(enemy_move.my_pos[a_i])] = 0;
						now_state.my_area_point -= global.field[enemy_move.my_pos[a_i].y][enemy_move.my_pos[a_i].x];
					}
					break;
				}
			}

			assert(now_state.action[0] >= 0 && now_state.action[0] < 3);
			assert(now_state.action[1] >= 0 && now_state.action[1] < 3);
			children << now_state;

		}
	}

	//並び替え
	sort(children.begin(), children.end(), [](const InfoAlphaBeta& left, const InfoAlphaBeta& right) {
		return (left.my_area_point - left.enemy_area_point) > (right.my_area_point - right.enemy_area_point);
	});

	//高速化の工夫
	//並列化

	int maxNum[4] = {};
	double total[9][9][3][3] = {};
	int total_sum[9][9][3][3] = {};

	for (int i = 0; i < children.size(); i++) {
		total_sum[children[i].first_dir[0]][children[i].first_dir[1]][children[i].action[0]][children[i].action[1]]++;
	}

	for (int i = 0; i < children.size(); i++) {
		ResultAlphaBeta result = funcSearchPerfect(children[i], global);
		if (result.score > 0) {
			assert(result.first_dir[0] >= 0 && result.first_dir[0] < 9);
			assert(result.first_dir[1] >= 0 && result.first_dir[1] < 9);
			assert(result.action[0] >= 0 && result.action[0] < 3);
			assert(result.action[1] >= 0 && result.action[1] < 3);

			assert(!std::isnan(1.0 / total_sum[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]]));
			total[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]] += 1.0 / total_sum[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]];

			if (total[maxNum[0]][maxNum[1]][maxNum[2]][maxNum[3]] < total[result.first_dir[0]][result.first_dir[1]][result.action[0]][result.action[1]]) {
				maxNum[0] = result.first_dir[0];
				maxNum[1] = result.first_dir[1];
				maxNum[2] = result.action[0];
				maxNum[3] = result.action[1];
				optimal = result;
			}
			/*
						if (abs(total[maxNum[0]][maxNum[1]][maxNum[2]][maxNum[3]] - 1.0) < 0.0000001) {
							optimal.score = 1;
							return optimal;
						}*/
		}
	}

	optimal.score = total[optimal.first_dir[0]][optimal.first_dir[1]][optimal.action[0]][optimal.action[1]];
	return optimal;
}