#include "Team.hpp"

bool Procon29::Team::loadAgents(String fileName)
{

	TextReader reader(fileName);
	if (reader.isOpened()) {
		String input = reader.readAll();
		if (input.size() < 16) {
			return false;
		}
		Array<String> singleSplit = input.split(':');
		Array<Array<String>> splitted;
		for (auto str : singleSplit) {
			splitted << str.split(' ').removed(U"");
		}
		int32 pointy = Parse<int32>(splitted.at(0).at(0));


		if (m_teamType == TeamType::MyTeam) {
			m_agents << Agent(Point(Parse<int32>(splitted.at(pointy + 1).at(1)) - 1, Parse<int32>(splitted.at(pointy + 1).at(0)) - 1));
			m_agents << Agent(Point(Parse<int32>(splitted.at(pointy + 2).at(1)) - 1, Parse<int32>(splitted.at(pointy + 2).at(0)) - 1));
		}
		if (m_teamType == TeamType::EnemyTeam) {
			m_agents << Agent(Point(Parse<int32>(splitted.at(pointy + 3).at(1)) - 1, Parse<int32>(splitted.at(pointy + 3).at(0)) - 1));
			m_agents << Agent(Point(Parse<int32>(splitted.at(pointy + 4).at(1)) - 1, Parse<int32>(splitted.at(pointy + 4).at(0)) - 1));
		}
		return true;
	}
	return false;
}

Procon29::Team::Team(Procon29::TeamType teamType, String fileName)
{
	m_score = 0;
	m_teamType = teamType;
	loadAgents(fileName);
}

Procon29::Team::Team(String initilizeString, bool isInvert)
{
	Array<String> spritter = initilizeString.split(':');
	m_agents << Agent(spritter.at(0));
	m_agents << Agent(spritter.at(1));
	m_score = Parse<int32>(spritter.at(2));
	m_teamType = static_cast<Procon29::TeamType>(Parse<int>(spritter.at(3)));
	if (isInvert) {
		if (m_teamType == TeamType::MyTeam) {
			m_teamType = TeamType::EnemyTeam;
		}
		else if (m_teamType == TeamType::EnemyTeam) {
			m_teamType = TeamType::MyTeam;
		}
	}
}

Procon29::Team::Team()
	:m_agents({ Agent(Point(-1,-1)),Agent(Point(-1,-1)) })
	, m_score()
	, m_teamType(TeamType::EnemyTeam)
{
}

void Procon29::Team::culcScore(const Field & field)
{
	Array<Array<bool>> visit(MaxFieldY, Array<bool>(MaxFieldX, false));

	Array<Point> q;

	int fieldSizeX = 12;
	int fieldSizeY = 12;

	for (auto y : step(MaxFieldY)) {
		if (field.boardGetter().at(y, 0).exist == false) {
			fieldSizeY = y + 1;
			break;
		}
	}
	for (auto x : step(MaxFieldX)) {
		if (field.boardGetter().at(0, x).exist == false) {
			fieldSizeX = x + 1;
			break;
		}
	}
	for (auto y : step(fieldSizeY)) {
		q.push_back({ 0, y });
		q.push_back({ fieldSizeX - 1,y });
	}
	for (auto x : step(fieldSizeX)) {
		q.push_back({ x , 0 });
		q.push_back({ x , fieldSizeY - 1 });
	}

	while (!q.isEmpty()) {

		auto now = q.front();
		q.pop_front();

		if (0 <= now.y && now.y < fieldSizeY && 0 <= now.x && now.x < fieldSizeX) {

			if (visit.at(now.y).at(now.x))
				continue;
			visit.at(now.y).at(now.x) = true;

			if (field.boardGetter().at(now.y, now.x).teamType != this->m_teamType) {

				q.push_back({ now.x - 1	,now.y });
				q.push_back({ now.x		,now.y + 1 });
				q.push_back({ now.x + 1 ,now.y });
				q.push_back({ now.x		,now.y - 1 });

			}

		}

	}

	int result = 0;

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			if (this->m_teamType != field.boardGetter().at(y, x).teamType && visit.at(y).at(x) == false) {
				result += abs(field.boardGetter().at(y, x).score);
			}
			if (field.boardGetter().at(y, x).teamType == this->m_teamType) {
				result += field.boardGetter().at(y, x).score;
			}
		}
	}

	m_score = result;

	return;
}

String Procon29::Team::drawSupporter(Procon29::Agent agent) const
{
	String ret;
	if (agent.selectedNextPoint) {
		if (agent.action == 0) {
			ret = U"指示完了(待機)";
		}
		else if (agent.action == 1) {
			ret = U"指示完了(移動)";
		}
		else if (agent.action == 2) {
			ret = U"指示完了(剥離)";
		}
	}
	else if (agent.nowSelect) {
		ret = U"選択中";
	}
	else {
		ret = U"未指示";
	}
	return ret;
}

void Procon29::Team::draw() const
{

	const Color enemyTeamColor = !isColorReverse ? Palette::Red : Palette::Blue;
	const Color myTeamColor = !isColorReverse ? Palette::Blue : Palette::Red;

	//agentにdrawを投げる
	m_agents.at(0).draw(m_teamType, 1);
	m_agents.at(1).draw(m_teamType, 2);

	if (m_teamType == Procon29::TeamType::MyTeam) {
		m_teamInfomation.movedBy(MaxFieldX * TileSize, 0).drawFrame(5, 0, myTeamColor);
		FontAsset(U"TeamInfomation")(VSComputer ? U"Computer" : U"MyTeam").draw(MaxFieldX * TileSize + 10, 10, Palette::Black);
		FontAsset(U"Fieldfont")(U"SCORE : ", m_score).draw(MaxFieldX * TileSize + 10, 70, Palette::Black);
		FontAsset(U"Fieldfont")(U"Agent 1  : ", drawSupporter(m_agents.at(0))).draw(MaxFieldX * TileSize + 10, 100, Palette::Black);
		//FontAsset(U"Fieldfont")(U"n:", m_agents.at(0).nowPoint, U"x:", m_agents.at(0).action).draw(MaxFieldX * TileSize + 380, 100, Palette::Black);
		FontAsset(U"Fieldfont")(U"Agent 2  : ", drawSupporter(m_agents.at(1))).draw(MaxFieldX * TileSize + 10, 130, Palette::Black);
		//FontAsset(U"Fieldfont")(U"n:", m_agents.at(1).nowPoint, U"x:", m_agents.at(1).action).draw(MaxFieldX * TileSize + 380, 130, Palette::Black);
	}
	else if (m_teamType == Procon29::TeamType::EnemyTeam) {
		m_teamInfomation.movedBy(MaxFieldX * TileSize + SideAreaX, 0).drawFrame(5, 0, enemyTeamColor);
		FontAsset(U"TeamInfomation")(VSComputer ? U"MyTeam" : U"EnemyTeam").draw(MaxFieldX * TileSize + 10 + SideAreaX, 10, Palette::Black);
		FontAsset(U"Fieldfont")(U"SCORE : ", m_score).draw(MaxFieldX * TileSize + 10 + SideAreaX, 70, Palette::Black);
		FontAsset(U"Fieldfont")(U"Agent 1  : ", drawSupporter(m_agents.at(0))).draw(MaxFieldX * TileSize + 10 + SideAreaX, 100, Palette::Black);
		//FontAsset(U"Fieldfont")(U"n:", m_agents.at(0).nowPoint, U"x:", m_agents.at(0).action).draw(MaxFieldX * TileSize + 380, 100, Palette::Black);
		FontAsset(U"Fieldfont")(U"Agent 2  : ", drawSupporter(m_agents.at(1))).draw(MaxFieldX * TileSize + 10 + SideAreaX, 130, Palette::Black);
		//FontAsset(U"Fieldfont")(U"n:", m_agents.at(1).nowPoint, U"x:", m_agents.at(1).action).draw(MaxFieldX * TileSize + 380, 130, Palette::Black);
	}

	FontAsset(U"Fieldfont")(isColorReverse ? U"反転色" : U"通常色").draw(MaxFieldX * TileSize + 20 * 10, 10 , myTeamColor);

	//Rect(64 * 12, 0, 300, 100).draw(Palette::Aliceblue);
	//m_agents[0].draw(Procon29::TeamType::MyTeam);
}

void Procon29::Team::update(bool colorFlag)
{
	isColorReverse = colorFlag;
	
}

bool Procon29::Team::onMouseAgent(Point pos)
{
	if (pos.x >= MaxFieldX || pos.y >= MaxFieldY || pos.x < 0 || pos.y < 0) {
		return false;
	}
	if (m_agents.at(0).nowPoint == pos) {
		//FontAsset(U"Fieldfont")(U"1が選択されました", *pos).draw(12 * 64 + 10, 12 * 64 / 2 + 160, Palette::Black);
		m_agents.at(0).nowSelect = true;
		m_agents.at(1).nowSelect = false;
		return true;
	}
	else if (m_agents.at(1).nowPoint == pos) {
		//FontAsset(U"Fieldfont")(U"2が選択されました", *pos).draw(12 * 64 + 10, 12 * 64 / 2 + 160, Palette::Black);
		m_agents.at(0).nowSelect = false;
		m_agents.at(1).nowSelect = true;
		return true;
	}
	return false;
}

void Procon29::Team::moving(Point pos, Procon29::TeamType tileColor)
{
	// pos : 移動先  tileColor : そのタイルの色
	if (m_agents.at(0).nowSelect) {
		m_agents.at(0).update(pos);
		if (m_teamType == tileColor) {
			m_agents.at(0).action = 1;
		}
		else if (TeamType::None == tileColor) {
			m_agents.at(0).action = 1;
			m_agents.at(0).nowSelect = false;
		}
		else {
			m_agents.at(0).action = 2;
			m_agents.at(0).nowSelect = false;
		}
	}
	else if (m_agents.at(1).nowSelect) {
		m_agents.at(1).update(pos);
		if (m_teamType == tileColor) {
			m_agents.at(1).action = 1;
		}
		else if (TeamType::None == tileColor) {
			m_agents.at(1).action = 1;
			m_agents.at(1).nowSelect = false;
		}
		else {
			m_agents.at(1).action = 2;
			m_agents.at(1).nowSelect = false;
		}
	}
}

Point Procon29::Team::nowSelectAgent() const
{
	if (m_agents.at(0).nowSelect) {
		return m_agents.at(0).nextPoint;
	}
	else if (m_agents.at(1).nowSelect) {
		return m_agents.at(1).nextPoint;
	}
	return { -1,-1 };
}

void Procon29::Team::changePeel()
{
	if (m_agents.at(0).nowSelect) {
		m_agents.at(0).action = 2;
		m_agents.at(0).nowSelect = false;
	}
	else if (m_agents.at(1).nowSelect) {
		m_agents.at(1).action = 2;
		m_agents.at(1).nowSelect = false;
	}
}

void Procon29::Team::changeStay()
{
	if (m_agents.at(0).nowSelect) {
		m_agents.at(0).action = 0;
		m_agents.at(0).nowSelect = false;
		m_agents.at(0).nextPoint = m_agents.at(0).nowPoint;
	}
	else if (m_agents.at(1).nowSelect) {
		m_agents.at(1).action = 0;
		m_agents.at(1).nowSelect = false;
		m_agents.at(1).nextPoint = m_agents.at(1).nowPoint;
	}
}

void Procon29::Team::changeMove()
{
	if (m_agents.at(0).nowSelect) {
		m_agents.at(0).action = 1;
		m_agents.at(0).nowSelect = false;
	}
	else if (m_agents.at(1).nowSelect) {
		m_agents.at(1).action = 1;
		m_agents.at(1).nowSelect = false;
	}
}

void Procon29::Team::invalidation()
{
	m_agents.at(0).nowSelect = false;
	m_agents.at(1).nowSelect = false;
}

void Procon29::Team::keySelectAgents(int32 num)
{
	if (num == 0) {
		m_agents.at(0).nowSelect = true;
	}
	else if (num == 1) {
		m_agents.at(1).nowSelect = true;
	}
}

const Procon29::TeamType Procon29::Team::teamTypeGetter() const
{
	return m_teamType;
}

const Array<Procon29::Agent>& Procon29::Team::agentsGeter() const
{
	return m_agents;
}

void Procon29::Team::moveAgents(Array<int32> actions, int32 team)
{
	m_agents.at(0).action = actions[team * 2];
	m_agents.at(1).action = actions[team * 2 + 1];
	for (auto & agent : m_agents) {
		agent.moveing();
	}

}

void Procon29::Team::forcedAgentPointChange(Point pos, bool superUserFlag)
{
	if (superUserFlag) {
		if (m_agents.at(0).nowSelect) {
			m_agents.at(0).nowPoint = pos;
		}
		if (m_agents.at(1).nowSelect) {
			m_agents.at(1).nowPoint = pos;
		}
		invalidation();
	}
}

void Procon29::Team::forcedGiveRationakityToTheField(Field & field,bool superUserFlag)
{
	if (superUserFlag) {
		field.set(m_agents.at(0).nowPoint, m_teamType);
		field.set(m_agents.at(1).nowPoint, m_teamType);
	}
}

s3d::String Procon29::Team::format() const
{
	return Format(m_agents[0].format(), U":", m_agents[1].format(), U":", m_score, U":", static_cast<int32>(m_teamType));
}

const int32 Procon29::Team::getScore() const
{
	return m_score;
}

void Procon29::Team::rotate90(const Field & field)
{
	//Team	reverse
	{
		for (int k = 0; k < MaxAgentNum; k++) {
			{
				const int32 y = field.boardSizeGetter().x - agentsGeter()[k].nowPoint.x - 1;
				const int32 x = agentsGeter()[k].nowPoint.y;
				this->m_agents[k].nowPoint.y = y;
				this->m_agents[k].nowPoint.x = x;
			}
			{
				const int32 y = field.boardSizeGetter().x - agentsGeter()[k].nextPoint.x - 1;
				const int32 x = agentsGeter()[k].nextPoint.y;
				this->m_agents[k].nextPoint.y = y;
				this->m_agents[k].nextPoint.x = x;
			}
		}
	}

}

