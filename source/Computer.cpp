//高速な点数計算プログラムや呼び出しの隠蔽など
#include <bitset>
#include <algorithm>
#include <unordered_map>
#include <utility>
#include <queue>
#include "Computer.hpp"

int Procon29::Computer::Search::calcScore(const Field & field, const std::bitset<144>& my_area) const
{
	bool visit[188] = {};

	int q_back = 0;
	int q_front = 0;
	unsigned char q[650];

	const int fieldSizeX = field.boardSizeGetter().x;
	const int fieldSizeY = field.boardSizeGetter().y;

	//char is 8bit and 1 byte.
#define XY_TO_CHAR(x,y) ((((x) & 0xF) << 4) | ((y) & 0xF))
#define CHAR_TO_X(c) (((c) >> 4) & 0xF)
#define CHAR_TO_Y(c) ((c) & 0xF)

	for (auto y : step(fieldSizeY)) {
		q[q_back] = XY_TO_CHAR(0, y);//std::make_pair(0, y);
		q_back++;
		q[q_back] = XY_TO_CHAR(fieldSizeX - 1, y);
		q_back++;
	}
	for (auto x : step(fieldSizeX)) {
		q[q_back] = XY_TO_CHAR(x, 0);
		q_back++;
		q[q_back] = XY_TO_CHAR(x, fieldSizeY - 1);
		q_back++;
	}

	while (q_front != q_back) {

		auto now = q[q_front];
		q_front++;

		const char x = CHAR_TO_X(now);
		const char y = CHAR_TO_Y(now);

		//unsignedだから0以下にはならない
		//		if (0 <= CHAR_TO_Y(now) && CHAR_TO_Y(now) < fieldSizeY && 0 <= CHAR_TO_X(now) && CHAR_TO_X(now) < fieldSizeX) {
		if (y < fieldSizeY && x < fieldSizeX && !visit[now]) {
			visit[now] = true;

			if (!my_area[x + y * 12]) {

				q[q_back] = XY_TO_CHAR(x - 1, y);
				q_back++;
				q[q_back] = XY_TO_CHAR(x, y + 1);
				q_back++;
				q[q_back] = XY_TO_CHAR(x + 1, y);
				q_back++;
				q[q_back] = XY_TO_CHAR(x, y - 1);
				q_back++;
			}
		}
		continue;
	}

	int result = 0;

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			assert(0 <= x + y * 12 && x + y * 12 < 144);
			if (!my_area[x + y * 12]/*teamType != field.boardGetter().at(y, x).teamType*/ && visit[XY_TO_CHAR(x,y)] == false) {
				result += abs(field.boardGetter().at(y, x).score);
			}
			if (my_area[x + y * 12]) {
				result += field.boardGetter().at(y, x).score;
			}
		}
	}

	return result;
}

int Procon29::Computer::Search::calcAreaScore(const Field & field, const std::bitset<144>& my_area) const
{
	//Array<Array<bool>> visit(MaxFieldY, Array<bool>(MaxFieldX, false));

	//Array<Point> q;
	/*
	const int fieldSizeX = field.boardSizeGetter().x;
	const int fieldSizeY = field.boardSizeGetter().y;

	for (auto y : step(MaxFieldY)) {
		if (field.boardGetter().at(y, 0).exist == false) {
			fieldSizeY = y;
			break;
		}
	}
	for (auto x : step(MaxFieldX)) {
		if (field.boardGetter().at(0, x).exist == false) {
			fieldSizeX = x;
			break;
		}
	}
	*/
	bool visit[188] = {};

	int q_back = 0;
	int q_front = 0;
	unsigned char q[650];

	const int fieldSizeX = field.boardSizeGetter().x;
	const int fieldSizeY = field.boardSizeGetter().y;

	//char is 8bit and 1 byte.
#define XY_TO_CHAR(x,y) ((((x) & 0xF) << 4) | ((y) & 0xF))
#define CHAR_TO_X(c) (((c) >> 4) & 0xF)
#define CHAR_TO_Y(c) ((c) & 0xF)

	for (auto y : step(fieldSizeY)) {
		q[q_back] = XY_TO_CHAR(0, y);//std::make_pair(0, y);
		q_back++;
		q[q_back] = XY_TO_CHAR(fieldSizeX - 1, y);
		q_back++;
	}
	for (auto x : step(fieldSizeX)) {
		q[q_back] = XY_TO_CHAR(x, 0);
		q_back++;
		q[q_back] = XY_TO_CHAR(x, fieldSizeY - 1);
		q_back++;
	}

	while (q_front != q_back) {

		auto now = q[q_front];
		q_front++;

		const char x = CHAR_TO_X(now);
		const char y = CHAR_TO_Y(now);

		//unsignedだから0以下にはならない
		//		if (0 <= CHAR_TO_Y(now) && CHAR_TO_Y(now) < fieldSizeY && 0 <= CHAR_TO_X(now) && CHAR_TO_X(now) < fieldSizeX) {
		if (y < fieldSizeY && x < fieldSizeX && !visit[now]) {
			visit[now] = true;

			if (!my_area[x + y * 12]) {

				q[q_back] = XY_TO_CHAR(x - 1, y);
				q_back++;
				q[q_back] = XY_TO_CHAR(x, y + 1);
				q_back++;
				q[q_back] = XY_TO_CHAR(x + 1, y);
				q_back++;
				q[q_back] = XY_TO_CHAR(x, y - 1);
				q_back++;
			}

		}

		continue;
	}

	int result = 0;

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			assert(0 <= x + y * 12 && x + y * 12 < 144);
			if (!my_area[x + y * 12]/*teamType != field.boardGetter().at(y, x).teamType*/ && visit[XY_TO_CHAR(x,y)] == false) {
				result += abs(field.boardGetter().at(y, x).score);
			}
		}
	}

	return result;

	/**
	for (auto y : step(fieldSizeY)) {
		q.push_back({ 0, y });
		q.push_back({ fieldSizeX - 1,y });
	}
	for (auto x : step(fieldSizeX)) {
		q.push_back({ x , 0 });
		q.push_back({ x , fieldSizeY - 1 });
	}

	while (!q.isEmpty()) {

		auto now = q.front();
		q.pop_front();

		if (0 <= now.y && now.y < fieldSizeY && 0 <= now.x && now.x < fieldSizeX) {

			if (visit.at(now.y).at(now.x))
				continue;
			visit.at(now.y).at(now.x) = true;

			//field.boardGetter().at(now.y, now.x).teamType != teamType
			if (!my_area[now.x + now.y * 12]) {

				q.push_back({ now.x - 1	,now.y });
				q.push_back({ now.x		,now.y + 1 });
				q.push_back({ now.x + 1 ,now.y });
				q.push_back({ now.x		,now.y - 1 });

			}

		}

	}

	int result = 0;

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			//teamType != field.boardGetter().at(y, x).teamType
			if (!my_area[x + y * 12] && visit.at(y).at(x) == false) {
				result += abs(field.boardGetter().at(y, x).score);
			}

			//if (field.boardGetter().at(y, x).teamType == teamType) {
			//	result += field.boardGetter().at(y, x).score;
			//}

		}
	}

	return result;
	*/
}

int Procon29::Computer::Search::calcTileScore(const Field & field, const std::bitset<144>& my_area) const
{

	int fieldSizeX = 12;
	int fieldSizeY = 12;

	for (auto y : step(MaxFieldY)) {
		if (field.boardGetter().at(y, 0).exist == false) {
			fieldSizeY = y;
			break;
		}
	}
	for (auto x : step(MaxFieldX)) {
		if (field.boardGetter().at(0, x).exist == false) {
			fieldSizeX = x;
			break;
		}
	}
	int result = 0;

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			if (my_area[x + y * 12]) {
				result += field.boardGetter().at(y, x).score;
			}
		}
	}

	return result;
}

int Procon29::Computer::Search::calcScore(const BYTE field[12][12], const std::bitset<144>& my_area) const
{

	bool visit[188] = {};

	int q_back = 0;
	int q_front = 0;
	char q[650];

	int fieldSizeX = 12;
	int fieldSizeY = 12;

	
	for (auto y : step(MaxFieldY)) {
		if (field[y][0] == -32) {
			fieldSizeY = y;
			break;
		}
	}
	for (auto x : step(MaxFieldX)) {
		if (field[0][x] == -32) {
			fieldSizeX = x;
			break;
		}
	}
	

	//char is 8bit and 1 byte.
#define XY_TO_CHAR(x,y) ((((x) & 0xF) << 4) | ((y) & 0xF))
#define CHAR_TO_X(c) (((c) >> 4) & 0xF)
#define CHAR_TO_Y(c) ((c) & 0xF)

	for (auto y : step(fieldSizeY)) {
		q[q_back] = XY_TO_CHAR(0, y);//std::make_pair(0, y);
		q_back++;
		q[q_back] = XY_TO_CHAR(fieldSizeX - 1, y);
		q_back++;
	}
	for (auto x : step(fieldSizeX)) {
		q[q_back] = XY_TO_CHAR(x, 0);
		q_back++;
		q[q_back] = XY_TO_CHAR(x, fieldSizeY - 1);
		q_back++;
	}

	while (q_front != q_back) {

		auto now = q[q_front];
		q_front++;

		const char & x = CHAR_TO_X(now);
		const char & y = CHAR_TO_Y(now);

		//unsignedだから0以下にはならない
//		if (0 <= CHAR_TO_Y(now) && CHAR_TO_Y(now) < fieldSizeY && 0 <= CHAR_TO_X(now) && CHAR_TO_X(now) < fieldSizeX) {
		if (y < fieldSizeY && x < fieldSizeX && !visit[now]) {
			visit[now] = true;

			if (!my_area[x + y * 12]/*field.boardGetter().at(CART_TO_Y(now), CART_TO_X(now)).teamType != teamType*/) {

				q[q_back] = XY_TO_CHAR(x - 1, y);
				q_back++;
				q[q_back] = XY_TO_CHAR(x, y + 1);
				q_back++;
				q[q_back] = XY_TO_CHAR(x + 1, y);
				q_back++;
				q[q_back] = XY_TO_CHAR(x, y - 1);
				q_back++;
			}

		}

		continue;
	}

	int result = 0;

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			assert(0 <= x + y * 12 && x + y * 12 < 144);
			if (!my_area[x + y * 12]/*teamType != field.boardGetter().at(y, x).teamType*/ && visit[XY_TO_CHAR(x,y)] == false) {
				result += abs(field[y][x]);
			}
			if (my_area[x + y * 12]) {
				result += field[y][x];
			}
		}
	}

	return result;

}

int Procon29::Computer::Search::calcAreaScore(const BYTE field[12][12], const std::bitset<144>& my_area) const
{
	bool visit[188] = {};

	int q_back = 0;
	int q_front = 0;
	char q[650];

	int fieldSizeX = 12;
	int fieldSizeY = 12;


	for (auto y : step(MaxFieldY)) {
		if (field[y][0] == -32) {
			fieldSizeY = y;
			break;
		}
	}
	for (auto x : step(MaxFieldX)) {
		if (field[0][x] == -32) {
			fieldSizeX = x;
			break;
		}
	}


	//char is 8bit and 1 byte.
#define XY_TO_CHAR(x,y) ((((x) & 0xF) << 4) | ((y) & 0xF))
#define CHAR_TO_X(c) (((c) >> 4) & 0xF)
#define CHAR_TO_Y(c) ((c) & 0xF)

	for (auto y : step(fieldSizeY)) {
		q[q_back] = XY_TO_CHAR(0, y);//std::make_pair(0, y);
		q_back++;
		q[q_back] = XY_TO_CHAR(fieldSizeX - 1, y);
		q_back++;
	}
	for (auto x : step(fieldSizeX)) {
		q[q_back] = XY_TO_CHAR(x, 0);
		q_back++;
		q[q_back] = XY_TO_CHAR(x, fieldSizeY - 1);
		q_back++;
	}

	while (q_front != q_back) {

		auto now = q[q_front];
		q_front++;

		const char & x = CHAR_TO_X(now);
		const char & y = CHAR_TO_Y(now);

		//unsignedだから0以下にはならない
		//		if (0 <= CHAR_TO_Y(now) && CHAR_TO_Y(now) < fieldSizeY && 0 <= CHAR_TO_X(now) && CHAR_TO_X(now) < fieldSizeX) {
		if (y < fieldSizeY && x < fieldSizeX && !visit[now]) {
			visit[now] = true;
			if (!my_area[x + y * 12]/*field.boardGetter().at(CART_TO_Y(now), CART_TO_X(now)).teamType != teamType*/) {
				q[q_back] = XY_TO_CHAR(x - 1, y);
				q_back++;
				q[q_back] = XY_TO_CHAR(x, y + 1);
				q_back++;
				q[q_back] = XY_TO_CHAR(x + 1, y);
				q_back++;
				q[q_back] = XY_TO_CHAR(x, y - 1);
				q_back++;
			}
		}
		continue;
	}

	int result = 0;

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			assert(0 <= x + y * 12 && x + y * 12 < 144);
			if (!my_area[x + y * 12]/*teamType != field.boardGetter().at(y, x).teamType*/ && visit[XY_TO_CHAR(x, y)] == false) {
				result += abs(field[y][x]);
			}
		}
	}

	return result;
}

int Procon29::Computer::Search::calcTileScore(const BYTE field[12][12], const std::bitset<144>& my_area) const
{
	int fieldSizeX = 12;
	int fieldSizeY = 12;

	for (auto y : step(MaxFieldY)) {
		if (field[y][0] == -32) {
			fieldSizeY = y;
			break;
		}
	}
	for (auto x : step(MaxFieldX)) {
		if (field[0][x] == -32) {
			fieldSizeX = x;
			break;
		}
	}

	int result = 0;

	for (auto y : step(fieldSizeY)) {
		for (auto x : step(fieldSizeX)) {
			assert(0 <= x + y * 12 && x + y * 12 < 144);
			if (my_area[x + y * 12]) {
				result += field[y][x];
			}
		}
	}

	return result;
}

void Procon29::Computer::calculate(const Teams& teams, const Field& field, const int & game_turn)
{
	const int same_count = 3;
	const size_t my_team = this->evaluate.my_team == TeamType::MyTeam ? 0 : 1;
	const size_t enemy_team = this->evaluate.my_team == TeamType::EnemyTeam ? 0 : 1;

	this->evaluate.game_turn = game_turn;
	this->evaluate.point_history[0] << teams[my_team].agentsGeter()[0].nowPoint;
	this->evaluate.point_history[1] << teams[my_team].agentsGeter()[1].nowPoint;
	
	int count = 0;
	
	for (int i = evaluate.point_history[0].size() - 1; i >= 0 && i >= evaluate.point_history[0].size() - same_count; i--) {
		if (this->evaluate.point_history[0][i] == this->evaluate.point_history[0].back() &&
			this->evaluate.point_history[1][i] == this->evaluate.point_history[1].back()) {
			count++;
		}
		else {
			break;
		}
	}
	if (same_count <= count && teams[my_team].getScore() <= teams[enemy_team].getScore()) {
		evaluate.lock = true;
	}
	else {
		evaluate.lock = false;
	}
	future = std::async(std::launch::async, [=]() {
		return search.beamSearch(teams, field, evaluate);
	});
}

Procon29::Computer::ComputerState Procon29::Computer::getState() const
{
	if (!this->future.valid())
		return ComputerState::Null;
	if (this->future.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
		return ComputerState::Ready;
	return ComputerState::Calc;
}

Procon29::Computer::ResultType Procon29::Computer::getResult()
{
	assert(this->future.valid());
	if (this->future.valid()) {
		ResultType result = this->future.get();
		if (result.size() != 0) {
			for (int i = 0; i < MaxAgentNum; i++)
				evaluate.before_calc_point[i] = Point((int)std::get<Array<Vec3>>(result.front())[i].x, (int)std::get<Array<Vec3>>(result.front())[i].y);
		}
		return result;
	}
	return ResultType();
}

Procon29::Computer::Computer()
{
}

Procon29::Computer::Computer(const int width, const TeamType teamtype, const bool useStupid)
{
	evaluate.beam_width = width;
	evaluate.my_team = teamtype;
	evaluate.useStupid = useStupid;
}

Procon29::Computer::Evaluate::Evaluate()
{
	this->beam_width = 100;
	this->my_team = TeamType::MyTeam;
	this->useStupid = false;
}

Procon29::Computer::Evaluate::Evaluate(const int width, const TeamType teamtype, const bool useStupid)
{
	this->beam_width = width;
	my_team = teamtype;
	this->useStupid = useStupid;
}

Procon29::Computer::Search::ResultAlphaBeta::ResultAlphaBeta()
{
	first_dir[0] = first_dir[1] = 0;
	score = -100000;
}
