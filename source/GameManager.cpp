#include "GameManager.hpp"

FilePath Procon29::GameManager::generateRandom()
{

	String writeFile = (U"random_field.txt");
	TextWriter tw(writeFile);

	int32 type = Random(0, 2);

	//最大マス幅１２
	//最小総合８０マス以上

	int32 height, width;

	do {
		height = Random(7, 12);
		width = Random(7, 12);
	} while (height * width < 80);

	tw.write(height, U" ", width);

	//負のマス10%

	Array<double> probability;

	for (int i = 0; i <= 16; i++) {
		//数式、付属のexcelで調整
		const double f = 0.0001210797*pow(i, 3) +
			-0.0040631445*pow(i, 2) +
			0.0340540830*pow(i, 1) +
			0.0122128681;
		if (i == 0)
			probability << f;
		else
			probability << f + probability.back();
	}

	assert((Abs(probability.at(16) - 1.0) < 0.0001));

	Grid<int> grid(width, height);

	for (auto& place : grid) {

		double value = Random();
		int32 score = -1;

		for (int i = 0; i <= 16; i++) {
			if (probability[i] > value) {
				score = i;
				break;
			}
		}

		assert(score != -1);

		if (RandomBool(0.1)) {
			place = score * -1;
		}
		else {
			place = score;
		}
	}

	//垂直に対称
	if (type == 0 || type == 1) {
		for (int32 y = 0; y < height; y++) {
			for (int32 x = 0; x < width / 2; x++) {
				grid.at(y, width - x - 1) = grid.at(y, x);
			}
		}
	}
	//水平に対称
	if (type == 0 || type == 2) {
		for (int32 y = 0; y < height / 2; y++) {
			for (int32 x = 0; x < width; x++) {
				grid.at(height - y - 1, x) = grid.at(y, x);
			}
		}
	}

	//点数の出力

	for (int32 y = 0; y < height; y++) {
		tw.write(U":");
		for (int32 x = 0; x < width; x++) {
			tw.write(grid.at(y, x));
			tw.write((x == width - 1) ? U"" : U" ");
		}
	}

	tw.write(U":");

	//エージェントの位置出力
	//水平かつ垂直に対称
	if (type == 0) {
		Point p(Random(1, width / 2), Random(1, height / 2));

		switch (Random(0, 2)) {
		case 0:
			tw.write(p.y, U" ", p.x, U":");
			tw.write(height - p.y + 1, U" ", p.x, U":");
			tw.write(p.y, U" ", width - p.x + 1, U":");
			tw.write(height - p.y + 1, U" ", width - p.x + 1, U":");
			break;
		case 1:
			tw.write(p.y, U" ", p.x, U":");
			tw.write(p.y, U" ", width - p.x + 1, U":");
			tw.write(height - p.y + 1, U" ", p.x, U":");
			tw.write(height - p.y + 1, U" ", width - p.x + 1, U":");
			break;
		case 2:
			tw.write(p.y, U" ", p.x, U":");
			tw.write(height - p.y + 1, U" ", width - p.x + 1, U":");
			tw.write(p.y, U" ", width - p.x + 1, U":");
			tw.write(height - p.y + 1, U" ", p.x, U":");
			break;
		}
	}

	//垂直に対称
	if (type == 1) {
		Point p1(Random(1, width / 2), Random(1, height));
		Point p2(Random(1, width / 2), Random(1, height));

		while (p1 == p2) {
			p1 = Point(Random(1, width / 2), Random(1, height));
			p2 = Point(Random(1, width / 2), Random(1, height));
		}

		tw.write(p1.y, U" ", p1.x, U":");
		tw.write(p2.y, U" ", p2.x, U":");
		tw.write(p1.y, U" ", width - p1.x + 1, U":");
		tw.write(p2.y, U" ", width - p2.x + 1, U":");
	}

	//水平に対称
	if (type == 2) {
		Point p1(Random(1, width), Random(1, height / 2));
		Point p2(Random(1, width), Random(1, height / 2));

		while (p1 == p2) {
			p1 = Point(Random(1, width), Random(1, height / 2));
			p2 = Point(Random(1, width), Random(1, height / 2));
		}

		tw.write(p1.y, U" ", p1.x, U":");
		tw.write(p2.y, U" ", p2.x, U":");
		tw.write(height - p1.y + 1, U" ", p1.x, U":");
		tw.write(height - p2.y + 1, U" ", p2.x, U":");
	}

	return FilePath(writeFile);
}

bool Procon29::GameManager::setting(Procon29::Teams& teams, Procon29::Field& field)
{

	Font font(60, Typeface::Medium);
	Stopwatch waitTimer(0s, true);
	String waitString = U".";
	if (IsRandom) {
		fileName = generateRandom();
	}
	else {
		fileName = U"qr_field.txt";

		if (!NotLoadedQR) {
			bool loadedQR = convertQRToText(fileName);
			assert(loadedQR);
		}
	}
	teams << Team(Procon29::TeamType::MyTeam, fileName);
	teams << Team(Procon29::TeamType::EnemyTeam, fileName);
	field = Field(fileName);
	m_turn = 1;

	//１ターン目対戦ログの出力を行う。
	outLog(field, teams);
	//１ターン目の点数の更新
	for (Team& i : teams) {
		i.culcScore(field);
	}

	m_turnTimer.restart();
	m_timer.restart();


	return true;
}

/*
@return
MyTeamWin : MyTeam
EnemyTeamWin : EnemyTeam
Draw : None
*/
Procon29::TeamType Procon29::GameManager::judgeWinner(const Teams & teams)
{
	if (teams.at(0).getScore() == teams.at(1).getScore()) {
		return TeamType::None;
	}
	else if (teams.at(0).getScore() > teams.at(1).getScore()) {
		return TeamType::MyTeam;
	}
	else if (teams.at(0).getScore() < teams.at(1).getScore()) {
		return TeamType::EnemyTeam;
	}

	return TeamType::Null;
}

/*
	**sample

	while (System::Update())
	{
		//Update
		outLog(field,teams);
	}
*/
void Procon29::GameManager::outLog(const Field & field, const Teams & teams)
{
	if (m_turn > m_Log.size()) {

		const TileGrid & tileGrid = field.boardGetter();

		m_Log.push_back(Log(tileGrid, teams));

		static int game_number = 0;

		if (m_turn == 1) {
			if (!FileSystem::Exists(U"history")) {
				FileSystem::CreateDirectories(U"history");
			}

			for (game_number = 0; FileSystem::Exists(U"history/game_" + ToString(game_number)); game_number++) {
			}

			FileSystem::CreateDirectories(U"history/game_" + ToString(game_number));
		}

		Image image;

		image.resize(Size(12, 12));

		for (const auto & y : step(MaxFieldY))
			for (const auto & x : step(MaxFieldY)) {
				if (tileGrid.at(y, x).exist == false) {
					image[y][x] = s3d::Color(0);
					continue;
				}

				switch (tileGrid.at(y, x).teamType) {
				case TeamType::None:
					image[y][x] = s3d::Color(120 + tileGrid.at(y, x).score);
					break;
				case TeamType::MyTeam:
					image[y][x] = s3d::Color(80 + tileGrid.at(y, x).score);
					break;
				case TeamType::EnemyTeam:
					image[y][x] = s3d::Color(160 + tileGrid.at(y, x).score);
					break;
				default:
					break;
				}

				for (const auto & i : step(MaxTeamNum)) {
					for (const auto & j : step(MaxAgentNum)) {
						if (teams.at(i).agentsGeter().at(j).nowPoint == s3d::Point(x, y)) {
							if (teams.at(i).teamTypeGetter() == TeamType::MyTeam)
								image[y][x] = s3d::Color(40 + tileGrid.at(y, x).score);
							if (teams.at(i).teamTypeGetter() == TeamType::EnemyTeam)
								image[y][x] = s3d::Color(200 + tileGrid.at(y, x).score);
						}
					}
				}
			}

		image.save(U"history/game_" + ToString(game_number) + U"/turn_" + ToString(m_turn) + U".png");

		if (MaxTurn == m_turn) {

			String winner;

			switch (judgeWinner(teams)) {
			case TeamType::MyTeam:
				winner = U"MyTeam";
				break;
			case TeamType::EnemyTeam:
				winner = U"EnemyTeam";
				break;
			case TeamType::None:
				winner = U"Draw";
				break;
			default:
				assert(judgeWinner(teams) == TeamType::Null);
				break;
			}

			TextWriter tw(U"history/game_" + ToString(game_number) + U"/result.txt");
			tw << U"winner:" << winner;
			tw << U"turn:" << m_turn;
			//teamのスコアを書き込む
			tw << teams.at(0).getScore() << U" " << teams.at(1).getScore();

		}

	}
}

/*
Sample
Main.cpp

bool loadedQR = convertQRToText(fileName);
assert(loadedQR);

*/
//16文字以上じゃないと読み取れない
bool Procon29::GameManager::convertQRToText(String fileName)
{
	//QRコードを撮影してテキストにコンバートする。
	Webcam webcam(0);

	webcam.setResolution(1200, 720);

	webcam.start();

	Image image;

	DynamicTexture texture;

	QRDecoder qrDecoder;

	while (System::Update()) {
		if (webcam.hasNewFrame()) {
			ClearPrint();

			webcam.getFrame(image);

			//image.adaptiveThresholded(AdaptiveMethod::Mean,3,0);

			Array<QRContent> contents;

			qrDecoder.decode(image, contents);

			for (const auto& content : contents) {
				content.quad.overwriteFrame(image, 6, Palette::Red);

				if (content.mode == QRMode::Alnum && content.text.size() > 16) {

					TextWriter tw(fileName);
					tw << addEnemyTeam(content.text);

					return true;
				}
			}

			image.mirror();

			texture.fill(image);
		}

		texture.draw();
		FontAsset(U"QRInfo")(U"QRコードを撮影してください。").draw();
	}

	return false;
}

//正常系のみ対処
String Procon29::GameManager::addEnemyTeam(String loadedText)
{

	auto splitedContent = loadedText.split(':');
	splitedContent.pop_back();
	String member1 = splitedContent.back();
	splitedContent.pop_back();
	String member2 = splitedContent.back();

	int32 height = ParseInt<int32>(splitedContent.at(0).split(' ').at(0));
	int32 width = ParseInt<int32>(splitedContent.at(0).split(' ').at(1));

	int32 member1Y = ParseInt<int32>(member1.split(' ').at(0));
	int32 member1X = ParseInt<int32>(member1.split(' ').at(1));

	int32 member2Y = ParseInt<int32>(member2.split(' ').at(0));
	int32 member2X = ParseInt<int32>(member2.split(' ').at(1));

	int32 enemy1Y = ParseInt<int32>(member1.split(' ').at(0));
	int32 enemy1X = ParseInt<int32>(member1.split(' ').at(1));

	int32 enemy2Y = ParseInt<int32>(member2.split(' ').at(0));
	int32 enemy2X = ParseInt<int32>(member2.split(' ').at(1));

	//水平フラグ
	bool horizontal = true;
	//垂直フラグ
	bool vertical = true;

	Grid<int> grid(width, height);

	Array<String> singleSplit = loadedText.split(':');
	Array<Array<String>> splitted;
	for (auto str : singleSplit) {
		splitted << str.split(' ').removed(U"");
	}
	Size m_boardSize = Size(Parse<int32>(splitted.at(0).at(1)), Parse<int32>(splitted.at(0).at(0)));

	for (auto point : step(m_boardSize)) {
		grid.at(point) = Parse<int32>(splitted.at(point.y + 1).at(point.x));
	}

	//垂直に対称
	for (int32 y = 0; y < height; y++) {
		for (int32 x = 0; x < width / 2; x++) {
			if (grid.at(y, width - x - 1) != grid.at(y, x)) {
				vertical = false;
			}
		}
	}
	//水平に対称
	for (int32 y = 0; y < height / 2; y++) {
		for (int32 x = 0; x < width; x++) {
			if (grid.at(height - y - 1, x) != grid.at(y, x)) {
				horizontal = false;
			}
		}
	}

	if (vertical && horizontal) {
		if (member1Y == member2Y) {
			enemy2Y = enemy1Y = height - member1Y + 1;
			enemy1X = member1X;
			enemy2X = member2X;
		}
		else if (member1X == member2X) {
			enemy2X = enemy1X = width - member1X + 1;
			enemy1Y = member1Y;
			enemy2Y = member2Y;
		}
		else {
			enemy1Y = member1Y;
			enemy1X = member2X;
			enemy2Y = member2Y;
			enemy2X = member1X;
		}
	}
	else if (vertical) {
		enemy1X = width - member1X + 1;
		enemy1Y = member1Y;
		enemy2X = width - member2X + 1;
		enemy2Y = member2Y;
	}
	else if (horizontal) {
		enemy1Y = height - member1Y + 1;
		enemy1X = member1X;
		enemy2Y = height - member2Y + 1;
		enemy2X = member2X;
	}
	else {
		assert(vertical || horizontal);
	}

	return loadedText + Format(enemy1Y, U" ", enemy1X, U":", enemy2Y, U" ", enemy2X, U":");
}

Stopwatch Procon29::GameManager::timerGetter() const
{
	return m_turnTimer;
}

int32 Procon29::GameManager::turnGetter() const
{
	return m_turn;
}

void Procon29::GameManager::update(Procon29::Teams& teams, Procon29::Field &field)
{

	Array<Point> nextPoints;
	Array<int32> actions;
	bool determine = true;

	//入力
	for (int i = 0; i < 2; i++) {
		nextPoints << teams.at(i).agentsGeter().at(0).nextPoint;
		nextPoints << teams.at(i).agentsGeter().at(1).nextPoint;
		actions << teams.at(i).agentsGeter().at(0).action;
		actions << teams.at(i).agentsGeter().at(1).action;
	}
	//重複消し切る
	while (determine) {
		determine = false;
		//重複削除
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i == j)continue;
				//移動
				if (actions.at(i) == 1) {
					//iの移動先がjのNowPointでjが剥離か待機でiを無効
					if (nextPoints.at(i) == teams.at(j / 2).agentsGeter().at(j % 2).nowPoint) {
						if (actions.at(j) == 0 || actions.at(j) == 2) {
							actions.at(i) = 0;
							determine = true;
						}
					}
					//移動先が同じで双方弾く
					else if (nextPoints.at(i) == nextPoints.at(j)) {
						actions.at(i) = 0;
						actions.at(j) = 0;
						determine = true;
					}
				}
				//剥離
				else if (actions.at(i) == 2) {
					//指定先にjが居る場合
					if (nextPoints.at(i) == teams.at(j / 2).agentsGeter().at(j % 2).nowPoint) {
						//jが剥離か待機の場合iが無効
						if (actions.at(j) == 0 || actions.at(j) == 2) {
							actions.at(i) = 0;
							determine = true;
						}
					}
					//移動先が同じだと弾く(一応)
					else if (nextPoints.at(i) == nextPoints.at(j)) {
						actions.at(i) = 0;
						actions.at(j) = 0;
						determine = true;
					}
				}
			}
		}

	}

	for (auto i : step(teams.size())) {
		teams.at(i).moveAgents(actions, i);
	}
	field.fieldUpdate(nextPoints, actions);
	m_turnTimer.restart();

	//点数の更新
	for (Team& i : teams) {
		i.culcScore(field);
	}
	//ターン経過
	m_turn++;
	//対戦ログの記録
	outLog(field, teams);
	isPreved = false;

}

void Procon29::GameManager::prev(Teams & teams, Field & field, Computer::ResultType & result, SaveData& saveData)
{
	if (!isPreved) {
		saveData.load(teams, field, result);
		m_turn--;
	}
	isPreved = true;
}

void Procon29::GameManager::timerIncrement()
{
	if (m_turn == MaxTurn - 1)
		return;
	this->m_turn++;
}

void Procon29::GameManager::timerDecrement()
{
	if (m_turn == 1)
		return;
	this->m_turn--;
}


Procon29::GameManager::Log::Log(const TileGrid& b, const Teams& t) : board(b), teams(t)
{
}

void Procon29::GameManager::drawScoreGraph()
{
	if (m_Log.size() == 0) {
		return;
	}

	const Point basePoint(50, Window::BaseHeight() - 100);
	const int32 thickness = 5;
	const int32 width = std::min((Window::Width() - 100) / MaxTurn, 20) * -1;
	const Color myTeamColor = Color(Palette::Blue, 150);
	const Color enemyTeamColor = Color(Palette::Red, 150);

	for (uint32 i = 0; i < m_Log.size() - 1; i++) {
		Line(basePoint - Point(width * i, m_Log[i].teams.at(0).getScore()), basePoint - Point(width * (i + 1), m_Log[i + 1].teams.at(0).getScore())).draw(thickness, myTeamColor);
		Line(basePoint - Point(width * i, m_Log[i].teams.at(1).getScore()), basePoint - Point(width * (i + 1), m_Log[i + 1].teams.at(1).getScore())).draw(thickness, enemyTeamColor);
	}

}