#pragma once

namespace Procon29 {

	enum class TeamType {
		None,

		MyTeam,

		EnemyTeam,

		Null,
	};

}