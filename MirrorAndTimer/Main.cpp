﻿
# include <Siv3D.hpp> // OpenSiv3D v0.2.8

void Main()
{
	constexpr Size BaseSize = { 1920,1080 };
	constexpr Size CameraSize = { 1280,720 };
	int32 limitTime = 5;

	Window::SetBaseSize(BaseSize);
	//ちゃんと書けよ...
	Graphics::SetFullScreen(true, BaseSize);

	for (const auto& info : System::EnumerateWebcams()) {
		Print << U"[{}] {} `{}`"_fmt(info.index, info.name, info.uniqueName);
	}

	Stopwatch turnTimer;
	Webcam camera(0);
	Print << camera.setResolution(CameraSize);
	Print << camera.getResolution();

	camera.start();
	const Wave wave(static_cast<uint8>(TinkleBell), C7, 0.8s, 1.0);
	const Audio audio(wave);
	DynamicTexture texture;
	
	const Font font(40);

	Rect supportLine({ BaseSize.x / 2 - 15,-2500 }, { 30,5000 });
	ColorF lineColor(ColorF(Palette::Lightblue), 0.7);
	Point lineCenter(BaseSize.x / 2, BaseSize.y / 2);

	const Rect timerRect({ 0,BaseSize.y / 15 * 14 }, { BaseSize.x, BaseSize.y / 15 });
	Rect timeRect({ 0,BaseSize.y / 15 * 14 }, { BaseSize.x, BaseSize.y / 15 });;

	while (System::Update()) {
		if (camera.hasNewFrame()) {
			camera.getFrame(texture);
		}
		if (KeySpace.down()) {
			if (turnTimer.isRunning()) {
				turnTimer.reset();
			}
			else {
				turnTimer.restart();
			}
		}

		if (KeyDown.pressed()) {
			lineCenter.y += 7;
		}
		if (KeyUp.pressed()) {
			lineCenter.y -= 7;
		}
		if (KeyEnter.down()) {
			lineCenter.y = BaseSize.y / 2;
		}

		texture.mirrored().resized(BaseSize).draw();
		timerRect.draw(turnTimer.isRunning() ? Palette::White : Palette::Red);
		if (turnTimer.isRunning()) {
			timeRect.set({ 0,BaseSize.y / 15 * 14 }, { (int32) ((double) (BaseSize.x) * (double) (turnTimer.ms() / (double) (limitTime * 1000))), BaseSize.y / 15 }).draw(Palette::Lightgreen);
			font(turnTimer.format(U"ss:xx")).drawAt(timerRect.center(), Palette::Black);
			if (turnTimer.s() >= limitTime) {
				audio.playOneShot();
				turnTimer.restart();
			}
		}
		supportLine.draw(lineColor)
			.rotatedAt(lineCenter, 45_deg).draw(lineColor)
			.rotatedAt(lineCenter, 45_deg).draw(lineColor)
			.rotatedAt(lineCenter, 45_deg).draw(lineColor);

		if (KeyQ.down())
			break;

	}

	camera.stop();

}
