#include "SenjuKaneko.hpp"

void Procon29::SenjuKaneko::update(Teams& teams, Field& field)
{
	SimpleGUI::CheckBox(colorReverse, U"�F���]����", Window::Size() - drawnTexture.size() - Point(60, 40));
	//SimpleGUI::CheckBox(placeReverse, U"�ʒu���]����", Window::Size() - drawnTexture.size() - Point(0, 40));

	size_t beforePositionIndex = positionIndex;

	SimpleGUI::RadioButtons(positionIndex, { U"��", U"��", U"��", U"�E" }, Window::Size() - drawnTexture.size() - Point(60, 0));

	if (beforePositionIndex != positionIndex)
	{
		const int convert[4] = { 2,0,1,3 };

		for (int i = 0; i < 4; i++) {
			if ((convert[beforePositionIndex] + i) % 4 == convert[positionIndex]) {
				break;
			}
			rotate90(teams, field);
		}
	}

	Team team = teams[0];

	int blackNum = colorReverse;
	int redNum = !colorReverse;

	blackAction = (Action)team.agentsGeter()[blackNum].action;
	redAction = (Action)team.agentsGeter()[redNum].action;

	const Array<Point> points =
		Array<Point>{ Point(-1,0),Point(-1,1),Point(0,1),Point(1,1),Point(1,0),Point(1,-1),Point(0,-1),Point(-1,-1) };

	if (team.agentsGeter()[blackNum].selectedNextPoint) {
		Point dist = team.agentsGeter()[blackNum].nextPoint - team.agentsGeter()[blackNum].nowPoint;
		for (int i = 0; i < 8; i++) {
			if (dist == points[i])
				blackDist = i;
		}
	}
	if (team.agentsGeter()[redNum].selectedNextPoint) {
		Point dist = team.agentsGeter()[redNum].nextPoint - team.agentsGeter()[redNum].nowPoint;
		for (int i = 0; i < 8; i++) {
			if (dist == points[i])
				redDist = i;
		}
	}
}

void Procon29::SenjuKaneko::draw() const
{
	drawnTexture.draw(Window::Size() - drawnTexture.size());

	if (blackAction == Action::Stay) {
		blackStayTex[0].draw(Window::Size() - drawnTexture.size() + Vec2(900, 650) * magnification - correction[0] * magnification);
	}
	else {
		Array<Texture> texBlack = blackAction == Action::Move ? blackMoveTex : blackPeelTex;
		texBlack[this->blackDist].draw(Window::Size() - drawnTexture.size() + Vec2(900, 650) * magnification - correction[this->blackDist] * magnification);
	}

	if (redAction == Action::Stay) {
		redStayTex[0].draw(Window::Size() - drawnTexture.size() + Vec2(1200, 650) * magnification - correction[0] * magnification);
	}
	else {
		Array<Texture> texRed = redAction == Action::Move ? redMoveTex : redPeelTex;
		texRed[this->redDist].draw(Window::Size() - drawnTexture.size() + Vec2(1200, 650) * magnification - correction[this->redDist] * magnification);
	}

	if (blackAction != Action::Stay && redAction != Action::Stay && this->blackDist == 4 && this->redDist == 4) {
		Array<Texture> texBlack = blackAction == Action::Move ? blackMoveTex : blackPeelTex;
		texBlack[this->blackDist].draw(Window::Size() - drawnTexture.size() + Vec2(900, 650) * magnification - correction[this->blackDist] * magnification);
	}
	//850,1250
}

Procon29::SenjuKaneko::SenjuKaneko()
{
	this->kaneko = Image(U"image/kaneko.jpg");
	Image blackMove(U"image/black_move.png");
	Image blackMove45(U"image/black_move45.png");
	Image blackStay(U"image/black_stay.png");
	Image blackPeel(U"image/black_peel.png");
	Image blackPeel45(U"image/black_peel45.png");
	Image redMove(U"image/red_move.png");
	Image redMove45(U"image/red_move45.png");
	Image redStay(U"image/red_stay.png");
	Image redPeel(U"image/red_peel.png");
	Image redPeel45(U"image/red_peel45.png");

	Array<Image> images;
	images << blackMove;
	images << blackPeel;
	images << redMove;
	images << redPeel;
	Array<Image> images45;
	images45 << blackMove45;
	images45 << blackPeel45;
	images45 << redMove45;
	images45 << redPeel45;

	for (int i = 0; i < 4; i++) {
		Array<Texture>& tex = i == 0 ? blackMoveTex : i == 1 ? blackPeelTex : i == 2 ? redMoveTex : redPeelTex;
		tex.push_back(Texture(images[i].scaled(this->magnification)));
		tex.push_back(Texture(images45[i].scaled(this->magnification)));
		tex.push_back(Texture(images[i].scaled(this->magnification).rotated90()));
		tex.push_back(Texture(images45[i].scaled(this->magnification).rotated90()));
		tex.push_back(Texture(images[i].scaled(this->magnification).rotated180()));
		tex.push_back(Texture(images45[i].scaled(this->magnification).rotated180()));
		tex.push_back(Texture(images[i].scaled(this->magnification).rotated270()));
		tex.push_back(Texture(images45[i].scaled(this->magnification).rotated270()));
	}

	blackStayTex << Texture(blackStay.scaled(this->magnification));
	redStayTex << Texture(redStay.scaled(this->magnification));

	this->correction << Point(1200, 350);
	this->correction << Point(750, 700);
	this->correction << Point(350, 1200);
	this->correction << Point(0, 750);
	this->correction << Point(0, 350);
	this->correction << Point(50, 0);
	this->correction << Point(350, 0);
	this->correction << Point(700, 0);

	Image drawnImage = kaneko;
	//850,650 : 1250,650
	drawnImage.scale(magnification);
	drawnImage.rotate90();
	drawnTexture = Texture(drawnImage);

	positionIndex = 1;
}

void Procon29::SenjuKaneko::rotate90(Teams & teams, Field & field)
{
	assert(teams.size() == 2);
	for (int i = 0; i < MaxTeamNum; i++)
		teams[i].rotate90(field);

	field.rotate90();
}
