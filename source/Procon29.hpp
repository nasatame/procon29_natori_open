#pragma once

# include <Siv3D.hpp> // OpenSiv3D v0.2.6
# include <future>
# include <chrono>
# include "TeamType.hpp"
# include "Tile.hpp"
# include <limits>

namespace Procon29 {

	//*******�萔*******	

	constexpr int MaxFieldX = 12;
	constexpr int MaxFieldY = 12;

	constexpr int TileSize = 64;
	constexpr int SideAreaX = 400;
	constexpr int buttonSizeX = 200;
	constexpr Size WindowSize{ MaxFieldX * TileSize + SideAreaX * 2, MaxFieldY * TileSize };

	constexpr int MaxTurn = 40 + 1;
	constexpr int MaxTeamNum = 2;
	constexpr int MaxAgentNum = 2;

	constexpr bool NotLoadedQR = false;
	constexpr auto TurnTimeLimit = 20s;
	constexpr auto FirstTurnTimeLimit = 40s;
	constexpr bool IsRandom = false;
	//VSComputet or ComputerVSComputer
	constexpr bool VSComputer = false;
	constexpr bool ComputerVSComputer = false;
	//Option(*ComputerVSComputer == true)
	constexpr bool AutoComputerVSComputer = false;

	//*******�֐�*******

	void LoadTexture();
	void LoadFont();

	void LoadData();
}
