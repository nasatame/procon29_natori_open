# Procon29 仙台名取　ソルバー #
-----------------------------
２９回高専プロコン・仙台名取・競技部門のソースコードです。 

公式サイト  
http://www.procon.gr.jp/  

# Developer #
-------------
Nasatame (@syumi_nasa)  
野菜ジュース(@yasaijuiceryota)   
ちか🐟(@chika_merc)  

# Requirements #
--------------
Visual Studio 2017    
OpenSiv3D 0.2.8  
その他の依存ライブラリはありません。    

# Building #
-------------
VSでOpenSiv3Dのプロジェクトを作り、"/source"のソースコードをプロジェクトに加えて下さい。  
"/App/image"を"プロジェクトフォルダ/App/image"に加えてください。  

# How to use #
--------------------------
### 起動
Procon29.hpp内の各種定数の設定を行ってください。  
主要なものとして、NotLoadedQRをfalseにするとQRコードを読み取るモードになります。  
trueにすると、前回読み込んだマップを開きます。（初回起動はエラー）  
ランダムに生成されたマップで起動したいときは、  

* NotLoadedQR = true;  
* IsRandom = true;  

にしておいてください。  

### 実行

![GUI Image](https://bitbucket.org/nasatame/procon29_natori_open/raw/3e74eabc9b4108ab046a2449a1245b5fcbb588c6/readme/example.png =200x200 )    
  
基本は、エージェントをクリックして、行先をポチポチするだけです。      
F7で、スーパーユーザーモード。      
左クリックでタイルの色変更。    
右クリックでエージェントを選択、もう一度クリックするとその場所に移動。    
F9で、チーム色反転。    
    
実はキーボードでも操作ができます。探してみてください。（投げやり）    
  
# Other #
------------------
もし質問、要望等あれば  
@syumi_nasa  
まで。
